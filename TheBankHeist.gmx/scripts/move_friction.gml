/// move_friction(speed, fric)

// Returns firction applied to speed while never passing zero. 

if (argument0 < 0)return min(argument0+argument1,0);
else if (argument0 > 0 )return max(argument0-argument1,0);

/*
    Ex 1:
    speed = move_friction(speed, 0.4);
    
    Ex 2:
    hspd = move_friction(hspd, 0.5);
*/


