/// scr_mouse_control(right ,left , down, up, analogdeadzone)

//Does your game need a mouse, here's a example of moving one without one

if !(argument0>argument4) 
&& !(argument1>argument4)
&& !(argument2>argument4)
&& !(argument3>argument4) return(0);

display_mouse_set(display_mouse_get_x(),display_mouse_get_y());   
var xx = (argument0-argument1)*mousemap_senh;      
var yy = (argument2-argument3)*mousemap_senv; 
display_mouse_set(display_mouse_get_x()+xx,display_mouse_get_y()+yy);
