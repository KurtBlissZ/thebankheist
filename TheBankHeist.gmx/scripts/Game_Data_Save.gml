/// Game_Data_Save()
// Game_Inventory() Game_Data_Load()
var i = ds_map_create();

// Player
ds_map_add(i, "health", health);
ds_map_add(i, "score", score);
ds_map_add(i, "lives", lives);
ds_map_add(i, "global.armor", global.armor);

// Dosh
ds_map_add(i, "global.dosh_van", global.dosh_van);
ds_map_add(i, "global.dosh_limit", global.dosh_limit);

// Inventory
ds_map_add(i, "global.dynamites", global.dynamites);
ds_map_add(i, "global.gun_index", global.gun_index);
ds_map_add(i, "global.gun_have", array_to_list(global.gun_have));
ds_map_add(i, "global.ammo", array_to_list(global.ammo));
ds_map_add(i, "global.gun_mag", array_to_list(global.gun_mag));

// Save And Destroy Ds Map 
file_delete("savedata");
ds_map_secure_save(i, "savedata");
ds_map_destroy(i);
