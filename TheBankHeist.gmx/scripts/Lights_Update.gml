// Activate 8 Clostest Lights
for (i=0; i<7; i+=1)
{
    global.light[i]=noone;
    global.light_dist[i]=-1;
};

// Find 8 closest Lights
var dist;
with(obj_light_point) {
    dist = distance_to_object(obj_player);
    
    for (i=0; i<7; i+=1)
    if (global.light_dist[i]==-1) || (global.light_dist[i]>dist) {
        global.light[i]=id;
        global.light_dist[i]=dist;
        break;
    }
}

// Define Lights
var inst;
for (i=0; i<7; i+=1){
    inst = global.light[i];
    if instance_exists(inst) {
        d3d_light_enable(i, true);
        d3d_light_define_point(i, inst.x, inst.y, inst.z, inst.range, inst.col);
    } else {
        d3d_light_enable(i, false);
    }
}

