/// Event_Editor_Camera_Draw()
m3d_pointer_setpos(_pointer, window_mouse_get_x(), window_mouse_get_y());

draw_set_colour(c_white);
/*d3d_set_projection_ext(
  x
, y
, z
, x+lengthdir_x(1,dir)*cos(degtorad(zdir))
, y+lengthdir_y(1,dir)*cos(degtorad(zdir))
, z+sin(degtorad(zdir))
, 0, 0, 1, 45, 16/9, 1, 32000);*/


// We set up the projection just like we would a normal 3D camera:
m3d_projection_define(_projection,   x
, y
, z
, x+lengthdir_x(1,dir)*cos(degtorad(zdir))
, y+lengthdir_y(1,dir)*cos(degtorad(zdir))
, z+sin(degtorad(zdir))
, 0, 0, 1, 45, 16/9)
// This optionally renders the scene. You can also use d3d_projection* instead.
m3d_projection_draw(_projection, 1, 32000)
// Next, we must convert the mouse to a directional 3D vector based
// on the projection:
var __vec = m3d_projection_calculate(_projection, _pointer);
// Once we have the vector, we can pass it into the desired plane and see
// if it collides:
var __collision = m3d_intersect_plane(__vec, 1, 0, 0, 0, 1, 0, 0, 0, 0);
if (__collision) { // If a collision, let’s draw an ellipsoid at the mouse’s position:
//var __x, __y, __z;
__x = m3d_projection_collision_get_x(_projection);
__y = m3d_projection_collision_get_y(_projection);
__z = m3d_projection_collision_get_z(_projection);
d3d_draw_ellipsoid(__x - 2, __y - 2, __z - 2, __x +2, __y +2, __z + 2, -1, 1, 1, 8);
}

with Obj_Object_Wall {
    Editor_D3d_Draw_Wall(height, tex, tex_repeat, tex_top, tex_top_repeat);
    //d3d_draw_block(bbox_right, bbox_bottom, 128, x, y, 0, 
    //  background_get_texture(tex), 1, 1);
}

with Obj_Object_Floor {
    
    d3d_draw_floor(x, y+sprite_height, 0, x+sprite_width, y,
    0, background_get_texture(tex), 
    sprite_width/tex_repeat, sprite_height/tex_repeat);

}

