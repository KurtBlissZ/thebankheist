/// Window_Draw(offx, offy)
offx = argument[0];
offy = argument[1];

Window_Draw_Position();

var titleh = 22;

// Draw Border
draw_rectangle_col(x-1-offx, y-1-offy, 
  bbox_right+1-offx, bbox_bottom+1-offy, c_black);

// Draw Window
draw_sprite_ext(sprite_index, image_index, x-offx, y-offy,
    image_xscale, image_yscale, image_angle, image_blend, image_alpha);

// Draw Title Bar
draw_rectangle_col(x-offx, y-offy, bbox_right-offx, y+titleh-offy, 
  iff(active, c_aqua, c_ltgray)); 
draw_set_color(iff(draging,c_blue,c_black));
draw_text(x+15-offx, y-offy, string(name) /*+ string(get_instance_number())*/);

//if draw_text_button(bbox_right-string_width(" X "), y, " X ") 
var str = " X ";
if draw_text_button_off(bbox_right-string_width(" X "), y, " X ", -1, -1, 
  offx, offy)
&& active {
    var i = ds_list_find_index(global.windows_list, id);
    ds_list_delete(global.windows_list, i);
    instance_destroy();
}

// Draw body
if script_exists(window_script) {
    script_execute(window_script,x, y+titleh);
}
