#define World_Create_Model
/// World_Create_Model(tex)
Model[argument0]     = d3d_model_create();
Model_Tex[argument0] = background_get_texture(argument0);

#define World_Add_Floor
/// World_Add_Floor()
if (DrawTypeIndex == DrawTypeStatic) begin

d3d_model_floor(other.Model[Tex], x, y+sprite_height, Z, 
x+sprite_width, y, Z, sprite_width/Hrp,sprite_height/Vrp);
destroy();

end ////////////////////////////////////////////////////////////////////

#define World_Add_Wall
/// World_Add_Wall()
if (DrawTypeIndex == DrawTypeStatic) begin

d3d_model_floor(other.Model[TexTop], x, y+sprite_height, Z2, 
x+sprite_width, y, Z2, sprite_width/Hrp,sprite_height/Vrp);

d3d_model_wall(other.Model[Tex],x+sprite_width,y+sprite_height,Z1,
x,y+sprite_height,Z2,sprite_width/Hrp,Z2/Vrp);

d3d_model_wall(other.Model[Tex],x,y,Z1,
x+sprite_width,y,Z2,sprite_width/Hrp,Z2/Vrp);

d3d_model_wall(other.Model[Tex],x+sprite_width,y,Z1,
x+sprite_width,y+sprite_height,Z2,sprite_height/Hrp,Z2/Vrp);

d3d_model_wall(other.Model[Tex],x,y+sprite_height,Z1,
x,y,Z2,sprite_height/Hrp,Z2/Vrp);

end ////////////////////////////////////////////////////////////////////