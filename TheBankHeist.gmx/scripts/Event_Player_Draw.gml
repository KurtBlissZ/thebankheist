///Draw
var _scale = run_scale/100;
var _xscale = image_xscale-_scale;
var _yscale = image_yscale-_scale;

//Shawdow
d3d_transform_set_translation(0, 0, z-1);
draw_sprite_ext(sprite_index,image_index,x+5,y+5,_xscale,_yscale,aim,c_black,0.6);
d3d_transform_set_identity();

//Player
d3d_transform_set_translation(0, 0, z);
draw_sprite_ext(sprite_index,image_index,x,y,_xscale,_yscale,aim,image_blend,image_alpha);
d3d_transform_set_identity();


