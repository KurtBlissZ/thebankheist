// Keyboard
con_left     = scc_add("Left"                , scc_type_keyboard, ord("A"));
con_right    = scc_add("Right"               , scc_type_keyboard, ord("D"));
con_up       = scc_add("Up"                  , scc_type_keyboard, ord("W"));
con_down     = scc_add("Down"                , scc_type_keyboard, ord("S"));
con_action   = scc_add("Action / Steal Money"  , scc_type_keyboard, vk_space);
con_action2  = scc_add("2nd Action / Enter Van", scc_type_keyboard, vk_control);
con_run      = scc_add("Run Toggle"          , scc_type_keyboard, vk_shift);
con_dynamite = scc_add("Throw Dynamite"      , scc_type_keyboard, ord("F"));
con_switch   = scc_add("Switch Weapons"      , scc_type_keyboard, vk_tab);
con_pistol   = scc_add("Switch to Pistol"    , scc_type_keyboard, ord("1"));
con_smg      = scc_add("Switch to M16A1"     , scc_type_keyboard, ord("2"));
van_sec      = Numb;
con_gas      = scc_add("Gas"                 , scc_type_keyboard, ord("W"));
con_reverse  = scc_add("Reverse"             , scc_type_keyboard, ord("S"));

// Controller
row2          = Numb;
con2_left     = scc_add("Left"                , scc_type_axisneg, gp_axislh);
con2_right    = scc_add("Right"               , scc_type_axispos, gp_axislh);
con2_up       = scc_add("Up"                  , scc_type_axisneg, gp_axislv);
con2_down     = scc_add("Down"                , scc_type_axispos, gp_axislv);
con2_action   = scc_add("Action"              , scc_type_button , gp_face3);
con2_action2  = scc_add("2nd Action / Van"    , scc_type_button , gp_face2);
con2_run      = scc_add("Run Toggle"          , scc_type_button , gp_face1);
con2_dynamite = scc_add("Throw Dynamite"      , scc_type_button , gp_shoulderr);
con2_switch   = scc_add("Switch Weapons"      , scc_type_button , gp_face4);
con2_pistol   = scc_add("Switch to Pistol"    , scc_type_button , gp_padl);
con2_smg      = scc_add("Switch to M16A1"     , scc_type_button , gp_padr);
con2_shoot    = scc_add("Shoot"               , scc_type_button , gp_shoulderrb);
con2_reload   = scc_add("Reload"              , scc_type_button , gp_shoulderlb);
con2_aimleft  = scc_add("Aim Left"            , scc_type_axisneg, gp_axisrh);
con2_aimright = scc_add("Aim Right"           , scc_type_axispos, gp_axisrh);
con2_aimup    = scc_add("Aim Up"              , scc_type_axisneg, gp_axisrv);
con2_aimdown  = scc_add("Aim Down"            , scc_type_axispos, gp_axisrv);
van_sec2      = Numb;
con2_gas      = scc_add("Gas"                 , scc_type_button,  gp_shoulderrb);
con2_reverse  = scc_add("Reverse"             , scc_type_button,  gp_shoulderlb);

