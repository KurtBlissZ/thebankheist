///Shooting / Reloading / Dynamite
var _gi      = global.gun_index;
if instance_exists(obj_scc) with obj_scc {
    var k_fire   = (mouse_check_button(mb_left) 
                   || check_button[con2_shoot])
                   && COMOFF;
    var k_reload = (mouse_check_button_released(mb_right) 
                   || check_button_released[con2_reload])
                   && COMOFF;
    var k_pistol = (check_button[con_pistol] || check_button[con2_pistol])
                    && COMOFF;
    var k_smg    = (check_button[con_smg] || check_button[con2_smg]) && COMOFF;
    var k_dyn    = (check_button_released[con_dynamite] || check_button_released[con2_dynamite])
                    && COMOFF;
} else {
    var k_fire   = (mouse_check_button(mb_left) || GP_rb() || GP_rt()) && COMOFF;
    var k_reload = mouse_check_button_released(mb_right) &&COMOFF;
    var k_pistol = keyboard_check_pressed(ord("1"))&&COMOFF;
    var k_smg    = keyboard_check_pressed(ord("2"))&&COMOFF;
    var k_dyn    = keyboard_check_released(ord("F"))&&COMOFF;
}

// Shooting
if k_fire
&& can_shoot
&& can_reload
&& (sprite_index == global.gun_sprite[_gi])
&& (global.gun_mag[_gi] > 0) {

    global.gun_mag[_gi]--;
    can_shoot         = false;
    alarm_shoot_delay = global.gun_firerate[_gi];
    audio_play_sound(global.gun_sound[_gi],10,0);
    instance_create(x,y,obj_casing);
    
    var gunexp = instance_create(x,y,obj_explosion);
    gunexp.owner       = id;
    gunexp.img_spd     = global.gun_exp_speed[_gi];
    gunexp.image_angle = aim;
    gunexp.len_add     = global.gun_effect_len[_gi];
    gunexp.dir_add     = global.gun_effect_dir[_gi];
    
    var bullet = instance_create(x,y,global.gun_bullet[_gi]);
    bullet.image_angle = aim;
    bullet.direction   = aim;
    bullet.spd         = global.gun_bullet_speed[_gi];
    bullet.bullet_dmg  = global.gun_bullet_damage[_gi];
} 

//Reloading
if k_reload
|| ((global.gun_mag[_gi] == 0) && k_fire) {
    event_user(0);
} 

//Throw Dynamite
if k_dyn
if (global.dynamites > 0) 
with instance_create(x,y,obj_dynamite_thrown) {
    motion_set(other.aim, 7);
    alarm_shoot_delay = room_speed*3;
    global.dynamites -= 1;
}

///Switch Weapons
if k_pistol && global.gun_have[gun_pistol] {
    global.gun_index = gun_pistol;
    sprite_index = global.gun_sprite[global.gun_index];
}
if k_smg && global.gun_have[gun_smg] {
    global.gun_index = gun_smg;
    sprite_index = global.gun_sprite[global.gun_index];
}

