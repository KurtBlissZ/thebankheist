///en_state_path_backup_init()
var dir_away = point_direction(obj_player.x, obj_player.y,x,y);
var xxstart = x; var yystart = y;
var _delta = global.Delta;

//find position to back up to
while(1) {
    if ((en_player_in_range())
    && (distance_to_object(obj_player) < backup_dist)) {
        x += lengthdir_x(4,dir_away)*_delta;
        y += lengthdir_y(4,dir_away)*_delta;
    }
    else break;
}

path_targetx = x; 
path_targety = y;
x = xxstart; 
y = yystart;

//start path
path_state = en_state_path_backup;
en_state_path_backup();
