/// Event_Window_Step()

// text field input
if active_input_field<>-1 {
    // update varible
    input_field[active_input_field] = keyboard_string;
    
    // check if done typing
    if kcp(vk_enter)
    || !active // window is inactive
    || !window_has_focus() { 
        active_input_field = -1;
    }
    
    // check if next
    if kcp(vk_tab) {
        active_input_field = 
          wrap(active_input_field+1, 0, array_length_1d(input_field) );
    }
}




