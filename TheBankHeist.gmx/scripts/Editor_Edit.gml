///Editor_Edit()
with Obj_Object {
    if kcp(vk_shift) active = false;
    if active click = false;
}

var emouse_x = Get_Editor_Mouse_X();
var emouse_y = Get_Editor_Mouse_Y();

// Left Click Create
var a = (mcp(mb_left) && (!kc(vk_shift)));
var b = (mc(mb_left) && kc(vk_shift));

if can_create
&& ( a || b ) 
&& click
&& (!position_meeting(emouse_x, emouse_y, Obj_Object))
&& (emouse_y > (view_yview+32)) {
    with instance_create(emouse_x, emouse_y, current_object)
    switch (object_index) {
        case Obj_Object_Wall:
            tex = other.current_tex;
            tex_top = other.current_tex_top;
            tex_repeat = other.current_tex_repeat;
            tex_top_repeat = other.current_tex_top_repeat;
            height = other.current_height;
            
            break;
        case Obj_Object_Floor:
            tex = other.current_tex;
            tex_repeat = other.current_tex_repeat;
            break;
    }
    click = false;
}

// Right Click Destroy
var a = (mcp(mb_right) && (!kc(vk_shift)) && click);
var b = (mc(mb_right) && kc(vk_shift));

if (can_delete==true)
&& (a || b) {
    click = false;
    with instance_position(emouse_x, emouse_y, Obj_Object) {
        if (object_index == Obj_Object_Wall) {
            Editor_unMerge_Wall(emouse_x, emouse_y, Obj_Object_Wall);         
        } else if (object_index == Obj_Object_Floor) {
            Editor_unMerge_Floor(emouse_x, emouse_y, Obj_Object_Floor);         
        } else {
            destroy();
        }
    }
}

// Select
if can_select
&& mcp(mb_left)
&& (!kc(vk_shift))
&& click
with (instance_position(emouse_x, emouse_y, Obj_Object)) {
    event_user(0);
}

// Bring up properties
if kcp(vk_control) {
    var v_oid = noone;
    
    with Obj_Object {
        if active {
            v_oid = id;
        }
    }
    
    if instance_exists(v_oid) {        
        with instance_create(100, 100, Obj_Window) {
            
            oid = v_oid;
            
            name = string(object_get_name(oid.object_index));

            window_script = Window_Body_Object_Properties;
            
            // Init Input Field Buttons
            active_input_field = -1;
            
            // Init fields
            input_field[IF.X] = oid.x;
            input_field[IF.Y] = oid.y;
            
            image_xscale = max(image_xscale, string_width(name) + 64);
            
        }
    }
}


