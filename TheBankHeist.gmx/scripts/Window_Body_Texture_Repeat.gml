/// Window_Body_Texture_Repeat()
if do_init {
    inputactive = 0;
    do_init = false;
}

inputstring = iff(inputactive,keyboard_string,Obj_Editor.current_tex_repeat);

var a = array(xx, yy, xx+64, yy+24);

draw_rectangle_col(a[0], a[1], a[2], a[3], iff(inputactive, c_aqua, c_ltgray));

if mouse_in_rectangle(a[0], a[1], a[2], a[3]) && active {
    if mcp(mb_left) {
        inputactive = true;
        keyboard_string = inputstring;
    }
}

if kcp(vk_enter) && active && inputactive {
    Obj_Editor.current_tex_repeat = real(keyboard_string);
    inputactive = false;
}

