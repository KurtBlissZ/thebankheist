/// draw_text_button(x, y, str, [highlight color], [text color])
var xx = argument[0];
var yy = argument[1];
var ss = argument[2];
if (argument_count>3)
    var hc = argument[3];
else
    var hc = c_red;
if (argument_count>4)
    var c  = argument[4];
else
    var c  = draw_get_color();
var _click = false;
if point_in_rectangle(mouse_x, mouse_y, xx, yy, 
xx + string_width(ss), yy + string_height(ss)) {
    _click = mouse_check_button_released(mb_left);
    draw_set_color(hc);
}else draw_set_color(c);
draw_text(xx, yy, ss);
return _click;


