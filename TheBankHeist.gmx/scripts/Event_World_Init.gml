/// Event_World_Init()
texture_set_repeat(true);
background_color = c_black;

/// Create Models   
var i=0,t;while background_exists(i){
    if (string_copy(background_get_name(i),1,4)=='tex_')
        World_Create_Model(i);
    i++;
}

global.mdl_lamp = d3d_model_create();
d3d_model_load(global.mdl_lamp, "Models\streetlight3.gmmod");

// Init World Objects
//with par_wall           World_Objects_Init();
//with par_wall_shootable World_Objects_Init();
//with par_floor          World_Objects_Init();

// Add Static World Objects To Models
with par_wall           World_Add_Wall();
with par_wall_shootable World_Add_Wall();
with par_floor          World_Add_Floor();
