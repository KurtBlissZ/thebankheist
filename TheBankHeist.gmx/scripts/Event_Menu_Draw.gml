/// Event_Menu_Draw()
draw_set_font(menu_font_select);  //Set font to get sep value
draw_set_valign(fa_middle);         
var sep  = string_height("|"); //seperation
var _col = draw_get_color();   //set the color back when we are done using it.
var xx   = x;                  //Current Menu Option Xpos
var yy   = y;                  //Current Menu Option Ypos
var s;                         //string
var h;                         //string_height

// Draw Menu Options
for (var i=0; i<=menu_count; i+=1)
{    
    s = menu_info_name[i];    //Menu options name
    h = string_height(s);     //String height

    // Select Menu Option With Mouse
    if (menu_info_action[i] <> MENU.ACTION_SKIP) //Skip if this isn't a menu option
    && (mouse_y > (yy - (h*0.5)))
    && (mouse_y < (yy + (h*0.5)))
    && ((mouse_x<>mouse_x_previous)||(mouse_y<>mouse_y_previous))
    && window_has_focus() 
            menu_select = i;

    // Highlight If Your On This Menu Options
    if (menu_select == i) {
        draw_set_font(menu_font_select);
        draw_set_color(menu_color_select);
    }
    else {
        draw_set_font(menu_font);
        draw_set_color(menu_color);
    }

    // draw menu option
    draw_text(xx, yy, s);

    // Move pos down for next menu option
    yy += sep;
};

// Play sound effect when switching options
if (select_previous<>menu_select) {
    select_previous = menu_select;
    audio_play_sound(snd_select, 10, false);
}

// Used to detect Mouse Movement
if window_has_focus() {
    mouse_x_previous = mouse_x;
    mouse_y_previous = mouse_y;
}

// Change the setting back to default
draw_set_color(_col);
draw_set_valign(fa_top);


