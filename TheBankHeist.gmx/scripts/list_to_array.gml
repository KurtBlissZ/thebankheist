/// list_to_array(str)
var list = ds_list_create();
var result = 0;
ds_list_read(list, argument0);
for(var i = 0; i < ds_list_size(list); i++) {
    result[i] = ds_list_find_value(list, i);
}
if is_array(result)
    return result;
else 
    debug_message("Not Array!");
