/// Menu_Action(action)

switch (argument0) // MENU.ACTIONs are defined in Menu_Enums script
{
    case MENU.ACTION_EXIT:      // Exits Game
    game_end(); 
    break;

    case MENU.ACTION_FULLSCREEN:// Toggles fullscreen
    if global.Can_Fullscreen {
        window_set_fullscreen(!window_get_fullscreen());
    } else {
        show_message("Please turn off AA and restart game if you want window mode");
        window_set_fullscreen(true);
    }
    break;
    
    case MENU.ACTION_RESTART:
    game_restart();
    break;

    // Built in actions ///////////////////////////////////////////////////
    case MENU.ACTION_PREVIOUS:  // Go to previous menu
    Menu_Sets(set_previous);
    break;
    
    
    case MENU.ACTION_MENU:      // Sets the menu
    Menu_Sets(menu_info_sub[menu_select]);
    break;
    
    case MENU.ACTION_SCRIPT:    // executes the menu option's script
    script_execute(menu_info_sub[menu_select]);
    break;
    
    case MENU.ACTION_ROOM:      // goes to the menu option's room
    room_goto(menu_info_sub[menu_select]);
    break;
    
    case MENU.ACTION_DEBUG:
    execute_command(string(menu_info_sub[menu_select]));
    break;
    
    case MENU.ACTION_SKIP:
    case MENU.ACTION_NONE:      // Do nothing, prevent sound from playing
    audio_stop_sound(snd_selected);
    break;
    
    case MENU.ACTION_VSYNC:
    Scr_Display_Vsync(!global.gVsync);
    menu_info_name[menu_select] = "Vsync ["+iff(global.gVsync, 'True', 'False')+"]";
    break;
    
    case MENU.ACTION_FXAA:
        switch(global.gAA) {
            case 0: Scr_Display_AA(2); break;
            case 2: Scr_Display_AA(4); break;
            case 4: Scr_Display_AA(8); break;
            case 8: Scr_Display_AA(0); break;
        } menu_info_name[menu_select] = "FXAA ["+iff(!global.gAA, 'Off', string(global.gAA))+"]";
    break;
    
    case MENU.ACTION_SMOOTHING:
    Scr_Interpolation();
    menu_info_name[menu_select] = "Smoothing ["+iff(!global.linear, 'Off', string(global.linear))+"]";
    break;
    
    case MENU.ACTION_SHOW_CIVILIANS:
    global.Show_Civilians = !global.Show_Civilians;
    menu_info_name[menu_select] = "Show Civilians ["+iff(global.Show_Civilians, "True", "False")+"]";
    break;
    
    // Show error if menu action doesn't exsist, 
        //you could comment this out if you like
    default: show_error("Action ["+string(argument0)+"] doesn't exists ", false);
}
