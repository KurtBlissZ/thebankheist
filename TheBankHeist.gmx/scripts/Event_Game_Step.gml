#define Event_Game_Step
/// Event_Game_Step()

///Pause ///Timer / Calling Reinforcements
if global.ingame {
    Game_InGame_Step();
}

// Debug Command
if kcp(vk_f12) {
    execute_command(get_string("execute_command",last_command));
}

//Toggle Command Prompt
if keyboard_check_pressed(192) { // ~ (before the 1 key)
    enter_command = !enter_command;
    keyboard_string = '';   
}

//Execute Command
if keyboard_check_pressed(vk_enter)
&& enter_command {
    logmsg='';
    include_string = true;
    execute_command(keyboard_string);
    if log=='cls' log = '';
    else log='#'+iff(include_string, keyboard_string, "")+logmsg+string(log);
    keyboard_string = '';
}


#define Game_InGame_Step
/// Game_InGame_Step()
var _delta = global.Delta;
if global.timer>0 global.timer-= 1 * _delta;
if global.call_reinf global.reinf+= 2 * _delta;

//Playing Sounds
if global.play_police_siren
{
    if !audio_is_playing (snd_police_siren)
        audio_play_sound (snd_police_siren,10,1);
        
    if !audio_is_playing (snd_police_scanner) 
        audio_play_sound (snd_police_scanner,10,1);
        
    global.reinf_per = false;
}

//Stopping Sounds and making enemies
if global.reinf >= 3000 && global.play_police_siren
{
    global.play_police_siren = false;
    audio_stop_sound(snd_police_siren);
    audio_stop_sound(snd_police_scanner);
    global.police_spr = true;
    spawn_reinf();
}

if keyboard_check_pressed(vk_tab) {
    mousex_setback = window_mouse_get_x();
    mousey_setback = window_mouse_get_y();
}else if keyboard_check_released(vk_tab) {
    window_mouse_set(mousex_setback,mousey_setback);
}

// Pause Game
if (keyboard_check_pressed(vk_escape) || GPany_start_pressed())
    Game_Pause();