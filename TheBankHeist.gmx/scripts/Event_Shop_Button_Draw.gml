/// Event_Shop_Button_Draw()
draw_sprite_ext(sprite_index, image_index, x, y,
image_xscale*xscale, image_yscale*yscale, image_angle, image_blend, image_alpha);

//Draw cost
draw_set_font(fnt_shop);
if point_in_rectangle(mouse_x, mouse_y,
bbox_left, bbox_top, bbox_right, bbox_bottom) {
    draw_text_colour(6, 128, '-$'+string(price), c_red, c_red, c_red, c_red, 1);
}
