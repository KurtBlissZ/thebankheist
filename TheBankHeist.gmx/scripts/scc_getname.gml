///scc_getname(id);
var return_string="", n;
n = argument0;

switch (con_ver[n]) {
    case 1: return_string = "jP"+string(con_controller[n])+" "; break;
    case 2: return_string = "gP"+string(con_controller[n])+" "; break;
    default: return_string = '';
} 

switch (con_type[n]) {
    case scc_type_button: 
        switch(con_button[n]) {
            case gp_face1:return_string+="A";break;
            case gp_face2:return_string+="B";break;
            case gp_face3:return_string+="X";break;
            case gp_face4:return_string+="Y";break;
            case gp_shoulderl:return_string+="LB";break;
            case gp_shoulderlb:return_string+="LT";break;
            case gp_shoulderr:return_string+="RB";break;
            case gp_shoulderrb:return_string+="RT";break;
            case gp_select:return_string+="Back";break;
            case gp_start:return_string+="Start";break;
            case gp_padu:return_string+="D-pad Up";break;
            case gp_padd:return_string+="D-pad Down";break;
            case gp_padl:return_string+="D-pad Left";break;
            case gp_padr:return_string+="D-pad Right";break;
            default: return_string+="["+string(con_button[n])+']';
        }return_string+=' Button';
        break;
    case scc_type_axispos: 
        switch(con_button[n]) {
            case gp_axislh: return_string+='Left Stick Hor '; break;
            case gp_axislv: return_string+='Left Stick Ver '; break;
            case gp_axisrh: return_string+='Right Stick Hor '; break;
            case gp_axisrv: return_string+='Right Stick Ver '; break;
            default: return_string += "Axis "+string(con_button[n]); break;
         } return_string+='+';
        break;
    case scc_type_axisneg: 
        switch(con_button[n]) {
            case gp_axislh: return_string+='Left Stick Hor '; break;
            case gp_axislv: return_string+='Left Stick Ver '; break;
            case gp_axisrh: return_string+='Right Stick Hor '; break;
            case gp_axisrv: return_string+='Right Stick Ver '; break;
            default: return_string += "Axis "+string(con_button[n]); break;
         } return_string+='-';
        break;
} 

if con_type[n] == scc_type_keyboard {
    return_string="";
    switch (con_button[n]) {                                //little help from the Customizable Controls Example By Nocturne 
        case vk_control: return_string+="Ctrl"; break;
        case 192: return_string+="`"; break;
        case vk_decimal: return_string+="Decimal"; break; 
        case vk_divide: return_string+="Divide"; break; 
        case vk_subtract: return_string+="Subtract"; break;
        case vk_lcontrol: return_string+="LCtrl"; break;
        case vk_rcontrol: return_string+="RCtrl"; break;
        case vk_shift: return_string+="Shift"; break;
        case vk_lshift: return_string+="LShift"; break;
        case vk_rshift: return_string+="RShift"; break;
        case vk_tab: return_string+="Tab"; break;
        case vk_enter: return_string+="Enter"; break;
        case vk_backspace: return_string+="Backspace"; break;
        case vk_space: return_string+="Space"; break;
        case vk_pageup: return_string+="PgUp"; break;
        case vk_pagedown: return_string+="PgDown"; break;
        case vk_end: return_string+="End"; break;
        case vk_home: return_string+="Home"; break;
        case vk_left: return_string+="Left"; break;
        case vk_right: return_string+="Right"; break;
        case vk_up: return_string+="Up"; break;
        case vk_down: return_string+="Down"; break;
        case vk_insert: return_string+="Ins"; break;
        case vk_delete: return_string+="Del"; break;
        case vk_numpad0: return_string+="Num0"; break;
        case vk_numpad1: return_string+="Num1"; break;
        case vk_numpad2: return_string+="Num2"; break;
        case vk_numpad3: return_string+="Num3"; break;
        case vk_numpad4: return_string+="Num4"; break;
        case vk_numpad5: return_string+="Num5"; break;
        case vk_numpad6: return_string+="Num6"; break;
        case vk_numpad7: return_string+="Num7"; break;
        case vk_numpad8: return_string+="Num8"; break;
        case vk_numpad9: return_string+="Num9"; break;
        case vk_f1: return_string+="F1"; break;
        case vk_f2: return_string+="F2"; break;
        case vk_f3: return_string+="F3"; break;
        case vk_f4: return_string+="F4"; break;
        case vk_f5: return_string+="F5"; break;
        case vk_f6: return_string+="F6"; break;
        case vk_f7: return_string+="F7"; break;
        case vk_f8: return_string+="F8"; break;
        case vk_f9: return_string+="F9"; break;
        case vk_f10: return_string+="F10"; break;
        case vk_f11: return_string+="F11"; break;
        case vk_f12: return_string+="F12"; break;
        case ord('A'): return_string+="A"; break;
        case ord('B'): return_string+="B"; break;
        case ord('C'): return_string+="C"; break;
        case ord('D'): return_string+="D"; break;
        case ord('E'): return_string+="E"; break;
        case ord('F'): return_string+="F"; break;
        case ord('G'): return_string+="G"; break;
        case ord('H'): return_string+="H"; break;
        case ord('I'): return_string+="I"; break;
        case ord('J'): return_string+="J"; break;
        case ord('K'): return_string+="K"; break;
        case ord('L'): return_string+="L"; break;
        case ord('M'): return_string+="M"; break;
        case ord('N'): return_string+="N"; break;
        case ord('O'): return_string+="O"; break;
        case ord('P'): return_string+="P"; break;
        case ord('Q'): return_string+="Q"; break;
        case ord('R'): return_string+="R"; break;
        case ord('S'): return_string+="S"; break;
        case ord('T'): return_string+="T"; break;
        case ord('U'): return_string+="U"; break;
        case ord('V'): return_string+="V"; break;
        case ord('W'): return_string+="W"; break;
        case ord('X'): return_string+="X"; break;
        case ord('Y'): return_string+="Y"; break;
        case ord('Z'): return_string+="Z"; break;
        case ord('0'): return_string+="0"; break;
        case ord('1'): return_string+="1"; break;
        case ord('2'): return_string+="2"; break;
        case ord('3'): return_string+="3"; break;
        case ord('4'): return_string+="4"; break;
        case ord('5'): return_string+="5"; break;
        case ord('6'): return_string+="6"; break;
        case ord('7'): return_string+="7"; break;
        case ord('8'): return_string+="8"; break;
        case ord('9'): return_string+="9"; break;
        default: return_string+="(" + string(con_button[n]) + ")";
    }
}

return return_string;
