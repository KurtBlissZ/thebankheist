/// Event_Menu_Init(menu)

// Defines all menu sets and menu actions
Menu_Enums(); 

// Used to remember prevous menu set and menu item
set_previous = -1;
current_menu = -1;

// Set menu set to default, if theres an menu set given use it instead
if (argument_count>0) Menu_Sets(argument[0]);
else Menu_Sets(MENU.SET_MAIN);

// Used to detect if mouse moved when 
mouse_x_previous = mouse_x;
mouse_y_previous = mouse_y;


// Gamepad
gp_can_up[50]   = 0;
gp_can_down[50] = 0;


