/// Event_Game_Draw_GUI()
if global.ingame {
    shader_set(shd_bright_contrast);
    shader_set_uniform_f(uni_brightness_amount, var_brightness_amount);
    shader_set_uniform_f(uni_contrast_amount, var_contrast_amount );
    draw_surface(application_surface, 0, 0);
    shader_reset();
}

// Debug Console // Event_Game_Step()
if enter_command begin

var xx = 896, yy = 64;

var txt = keyboard_string+'|'+string(log);
draw_set_font(fnt_console);

//Recatangle behind text
draw_set_alpha(0.8);
draw_set_color(c_silver);
draw_rectangle(xx,yy
,max(string_width(txt),240)+xx
,max(string_height(txt),0)+yy,0);

draw_set_alpha(1);
draw_set_color(c_black);
draw_rectangle(xx-1,yy-1
,max(string_width(txt),240)+xx+1
,max(string_height(txt),0)+yy+1,1);

//Text
draw_set_alpha(1);
draw_set_color(c_black);
draw_text(xx,yy,txt);

end
