///en_state_chase()
if !instance_exists(obj_player) {
    direction = aim;
    state = en_state_idle;
    //path_speed = 0;
    path_spd = 0;
    exit;
}

//Build Path
if !delay_path_refresh {
    en_execute_path();
    delay_path_refresh = 30;
} else delay_path_refresh--;

//Manage distance with player
if distance_to_object(obj_player) < backup_dist {
    en_state_path_backup_init();
    exit;
} else if ((distance_to_object(obj_player) < stop_dist) && en_player_in_sight()) {
    //path_speed = 0;
    path_spd = 0;
}

//Aim towards player
var dir_to_player = point_direction(x,y,obj_player.x,obj_player.y);
aim += turn_towards_direction(dir_to_player,5,aim);

//Shoot
if !delay_shoot {
    if en_player_can_shoot() {
        delay_shoot_reaction = 50;
        delay_shoot = 120;
    }
} else delay_shoot--;

if delay_shoot_reaction==0 {
    alarm[2] = irandom(7);
} delay_shoot_reaction=max(delay_shoot_reaction-1,-1);
