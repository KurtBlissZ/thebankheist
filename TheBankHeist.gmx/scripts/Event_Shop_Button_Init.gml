/// Event_Shop_Button_Init()
grow_min = 1;
grow_max = 1.5;
grow_rate = 0.06;
grow = grow_min;
xscale = 1;
yscale = 1;
image_speed = 0;
image_xscale = 0.5;
image_yscale = 0.5;

switch object_index {
    case obj_sp_ammo:
    price = 600;
    image_xscale = 0.25;
    image_yscale = 0.25;
    break;
    
    case obj_sp_dynamite:
    price = 1000;
    if global.dynamite_unlk {
        sprite_index = spr_demo_unlk;
        image_index = 0;
    }
    break;
    
    case obj_sp_ak:
    price = 0;
    image_index = 2;
    image_xscale = 0.3;
    image_yscale = 0.3;
    break;
    
    case obj_sp_pistol:
    price = 1500;
    if global.pistol_unlk {
          sprite_index = spr_weapons_unlk2;
          image_index = 0;
    }
    break;
    
    case obj_sp_rifle:
    price = 6500;
    image_index = 1;
    image_xscale = 0.3;
    image_yscale = 0.3;
    if global.rifle_unlk {
        sprite_index = spr_weapons_unlk2;
        image_index = 1;
    }
    break;
    
    case obj_sp_shotty:
    price = 0;
    image_index = 3;
    image_xscale = 0.3;
    image_yscale = 0.3;    
    break;
}
