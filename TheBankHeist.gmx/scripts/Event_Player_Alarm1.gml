#define Event_Player_Alarm1
/// Event_Player_Alarm1

/// Finish Reloading
can_shoot = true;
can_reload = true;
var _gi = global.gun_index;
var _at = global.gun_ammo_type[_gi]
var _a = global.ammo[_at];
var _add = min(global.gun_mag_size[_gi]-global.gun_mag[_gi], _a);
global.ammo[_at] -= _add;
global.gun_mag[_gi] += _add;


#define Event_Player_Alarm2
/// Event_Player_Alarm2

/// Set sprite back to gun
sprite_index = global.gun_sprite[global.gun_index];



#define Event_Player_Alarm3
/// Event_Player_Alarm3

///Set color back to normal. 
image_blend = c_white;