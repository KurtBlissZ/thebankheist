/// instance_singleton_ext(obj1, obj2, ...)
var a,b,i=argument_count;
repeat i {
    i--;
    if instance_exists(argument[i])
        a[i]=instance_find(argument[i],0);
    else
        a[i]=instance_create(0,0,argument[i]);
}return a;
