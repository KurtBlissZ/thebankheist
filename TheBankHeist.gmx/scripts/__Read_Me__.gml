// This script does nothing!
// Use Middle Mouse Button To Open Up Anything Here!

// Init!
Event_Game_Create(); 
Game_Create_Inventory();
//Where you set textures to wall n floor objects
World_Objects_Init();
//Overide settings by using these in the instance creation code 
//All arguments are optional, but they have to be in order 
                                //and you can't skip any.
//Also use the Default macro for arguments you don't want to change
World_Init_Wall(tex, DrawType, ztop, hrp, vrp, zbottom, toptex);
World_Init_Floor(tex, DrawType, hrp, vrp, z);

// Game Hud
Event_Game_Draw();
Game_Draw_Hud();
Game_Draw_Switch_Weapons();

// Menus 
Menu_Enums(); // Enums!
Menu_Sets(0); // Menus, Pause, Titlescreen

//////////////////////////////////////////////

// Press F12 to enter commands
execute_command("debug");

    
