/// Editor_unMerge_Floor(x,y,obj)
with instance_position(argument0, argument1, argument2) {
    instance_destroy();
    
    var cell_last_x = floor(((bbox_right) - x) / 32);
    var cell_last_y = floor(((bbox_bottom) - y) / 32);
    var cell_remove_x = ((snap(argument0, 32) - x) / 32);
    var cell_remove_y = ((snap(argument1, 32) - y) / 32);    
    
    for (var xx=0, yy=0; xx<=cell_last_x; xx++)
    for (yy=0; yy<=cell_last_y; yy++) 
    if not ((xx==cell_remove_x) && (yy==cell_remove_y))
    && Editor_Matching_Floors() {
        with instance_create(x+(32*xx), y+(32*yy), argument2) {
            Editor_Matching_Set_Floors();
        }
    }
    
    Editor_Merge_Floors();
}

