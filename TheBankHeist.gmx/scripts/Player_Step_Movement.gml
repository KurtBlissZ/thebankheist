/// Movement / Collision
if instance_exists(obj_scc)
with obj_scc {
    // Keyboard
    var k_d     = check_button[con_right] && COMOFF;
    var k_a     = check_button[con_left] && COMOFF;
    var k_s     = check_button[con_down] && COMOFF;
    var k_w     = check_button[con_up] && COMOFF;
    var k_shift = check_button[con_run] && COMOFF;
    
    // Gamepad
    var g_a     = check_button[con2_run] && COMOFF;
    
    if  COMOFF {
    var g_lsh   = check_analog_direct[con2_right]-check_analog_direct[con2_left];
    var g_lsv   = check_analog_direct[con2_down] -check_analog_direct[con2_up];   
    var g_ls    = ((abs(g_lsh)>0.4)  || (abs(g_lsv)>0.4)); 
    var g_rsh   = check_analog_direct[con2_aimright]-check_analog_direct[con2_aimleft];
    var g_rsv   = check_analog_direct[con2_aimdown] -check_analog_direct[con2_aimup];   
    var g_rs    = ((abs(g_rsh)>0.4)  || (abs(g_rsv)>0.4));
    var g_rsdir = point_direction(0, 0, g_rsh, g_rsv);
    } else {
    var g_lsh=0,g_lsv=0,g_ls=0,g_rsh=0,g_rsv=0,g_rs=0,g_rsdir=0;
    }    
} else {
    // Keyboard
    var k_d     = keyboard_check(ord("D")) && COMOFF;
    var k_a     = keyboard_check(ord("A")) && COMOFF;
    var k_s     = keyboard_check(ord("S")) && COMOFF;
    var k_w     = keyboard_check(ord("W")) && COMOFF;
    var k_shift = keyboard_check_pressed(vk_shift) && COMOFF;
    
    // Gamepad
    var g_a     = GP_a(); 
    var g_ls    = GP_leftStrick();
    var g_lsh   = gamepad_axis_value(global.gamepad_device,gp_axislh);
    var g_lsv   = gamepad_axis_value(global.gamepad_device,gp_axislv);
    var g_rs    = GP_rightStrick();
    var g_rsdir = GP_rightStickDir();    
}

MoveX = 0;
MoveY = 0;
var _Delta = global.Delta;
if running var spd = 6;
else var spd = 4;

// Controls (Move and Aim)
if global.gamepad { // Gamepad
    running = g_a; 
    if g_ls { 
        MoveX = spd*g_lsh*_Delta;
        MoveY = spd*g_lsv*_Delta;
    } if (g_rs && !(global.switching ))
        aim = g_rsdir; 
    else if (((abs(MoveX)>0.4) || (abs(MoveY)>0.4)) && !(global.switching))
        aim = point_direction(x, y, x+MoveX, y+MoveY);
} else { // Keyboard and Mouse
    if !(global.switching) aim = point_direction(x, y, mouse_x, mouse_y);
    MoveX = spd*(k_d-k_a)*_Delta; 
    MoveY = spd*(k_s-k_w)*_Delta;
    if running {    
        if (k_shift || mouse_check_button(mb_left) || (MoveX==0 && MoveY==0))
            running = false;
    } else if k_shift
        running = true; 
}

//Collision 
while (abs(MoveX)>0) {
    if abs(MoveX)<1 {
        if !place_meeting(x+(sign(MoveX) * frac(MoveX)), y, par_solid) {
            x += sign(MoveX) * frac(MoveX);
            MoveX -= sign(MoveX) * frac(MoveX);
        } else {
            break;
        }
    } else if !place_meeting(x+sign(MoveX), y, par_solid) {
        x += sign(MoveX);
        MoveX -= sign(MoveX);
    } else {
        MoveX = 0;
        break;
    }
}

while (abs(MoveY)>0) {
    if abs(MoveY)<1 {
        if !place_meeting(x, y+(sign(MoveY) * frac(MoveY)), par_solid) {
            y += sign(MoveY) * frac(MoveY);
            MoveY -= sign(MoveY) * frac(MoveY);
        } else {
            break;
        }
    } else if !place_meeting(x, y+sign(MoveY), par_solid) {
        y += sign(MoveY);
        MoveY -= sign(MoveY);
    } else {
        MoveY = 0;
        break;
    }
}

/*while place_meeting(x+MoveX, y, par_solid) 
  || collision_line(x, y, x+MoveX, y, par_solid, false, true) {
    if (abs(MoveX) < 1) 
        MoveX-=sign(MoveX)*frac(MoveX);
    else 
        MoveX -= sign(MoveX);
    if (MoveX==0) break;
} x += MoveX; 

while place_meeting(x, y+MoveY, par_solid)
  || collision_line(x, y, x, y+MoveY, par_solid, false, true) {
    if (abs(MoveY) < 1) 
        MoveY-=sign(MoveY)*frac(MoveY);
    else 
        MoveY -= sign(MoveY);
    if (MoveY==0) break;
} y += MoveY;*/

// Play walking sounds
if (abs(MoveX) || abs(MoveY)) {
    if !delay_footstep {
        if (spd > 4) 
            delay_footstep = 30;
        else 
            delay_footstep = 60;
        audio_play_sound(snd_footstep,10,0);
    } else delay_footstep -= 1 * _Delta;
}

//Running effect (Scales sprite)
if (running || (run_scale<=0)) {
    run_scale += run_scale_spd * _Delta;
    if (run_scale>=10) {
        run_scale = 10;
        run_scale_spd = -run_scale_spd;// * _Delta;
    } else if (run_scale<=0) {
        run_scale = 0;
        run_scale_spd = -run_scale_spd;// * _Delta;
    }
}

