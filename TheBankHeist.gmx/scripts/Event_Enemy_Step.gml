/// Event_Enemy_Step()

///Execute state
if hp > 0 {
    script_execute(state);
    if path_exists(path_index)
        path_position += (path_spd / path_get_length(path_index)) * global.Delta;
} else {
    //Die!
    instance_create(x,y,obj_blood);
    instance_destroy();
}

