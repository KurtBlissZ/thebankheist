///d3d_fog(enable,color,start,end);
//
//d3d_fog(); toggles
//d3d_fog(enable); 
//d3d_fog(enable,color,start,end);
//

switch (argument_count)
{
    case 0: global.fg_fog = !global.fg_fog; break;
    case 1: global.fg_fog = argument[0]; break;
    default: 
        global.fg_fog = argument[0];
        global.fg_color = argument[1];
        global.fg_start = argument[2];
        global.fg_end = argument[3];
}

d3d_set_fog(global.fg_fog,global.fg_color,global.fg_start,global.fg_end);
