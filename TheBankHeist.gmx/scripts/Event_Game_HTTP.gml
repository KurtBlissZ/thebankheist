/// Event_Game_HTTP()
if ds_map_find_value(async_load, "id") == file {
    var status = ds_map_find_value(async_load, "status");
    if (status == 0) {
        show_debug_message("Updater Ini Downloaded.");
        ini_open(filename);
        var ver = ini_read_string("Information","Newest_Version","0.0.0.0");
        var updateurl = ini_read_string("Information","UpdateURL","http://tiny.cc/tbh");
        ini_close();
        
        // Check for update
        if GM_Version_Check(GM_version, ver) 
        && show_question("New version "+string(ver)+" is avaible!#"+'Close game and goto forum page to download update?'){
            ExecuteShell('explorer "'+string(updateurl)+'"', false);
            game_end();
        }
    } else show_debug_message("########################## Updater Download Failed!! ###########################");
}

