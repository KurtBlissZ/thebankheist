/// scr_controls_detect_joystick(control);
if keyboard_check(vk_escape) return(0);

var i; 
for(i=0;joystick_exists(i);i+=1) {
    for(ii=0;ii<=joystick_buttons(i);ii+=1) {
        if joystick_check_button(i,ii) {
            con_type[argument0] = scc_type_button; 
            con_button[argument0] = ii; 
            con_controller[argument0] = i; //sets controller/device to this control
            return(1);
        }    
    }
    //checks joystick's axis
    if joystick_axes(i) >= 1
    if joystick_xpos(i)<-0.5 //just to dectect, goes from -1 to 1, a xbox-controller can be very slighty touched and change axis 2 prevent doing so and moving do this
    {
        con_type[argument0] = scc_type_axisneg; 
        con_button[argument0]=0
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 1
    if joystick_xpos(i)>0.5 //just to dectect, goes from -1 to 1, a xbox-controller can be very slighty touched and change axis 2 prevent doing so and moving do this
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=1   
        con_controller[argument0]=i; 
        return(1);
    }
    
    if joystick_axes(i) >= 2
    if joystick_ypos(i)<-0.5
    {
        con_type[argument0] = scc_type_axisneg;
         con_button[argument0]=2    
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 2
    if joystick_ypos(i)>0.5
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=3    
        con_controller[argument0]=i;
        return(1);
    }
    
    if joystick_axes(i) >= 3 //for Microsoft PC-joystick driver, this is actually left trigger and right trigger on a xbox controller
    if joystick_zpos(i)<-0.5
    {
        con_type[argument0] = scc_type_axisneg;
         con_button[argument0]=4    
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 3 //for Microsoft PC-joystick driver, this is actually left trigger and right trigger on a xbox controller
    if joystick_zpos(i)>0.5
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=5    
        con_controller[argument0]=i;
        return(1);
    }
    
    if joystick_axes(i) >= 4
    if joystick_rpos(i)<-0.5
    {
        con_type[argument0] = scc_type_axisneg;
         con_button[argument0]=6    
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 4
    if joystick_rpos(i)>0.5
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=7    
        con_controller[argument0]=i;
        return(1);
    }
    
    if joystick_axes(i) >= 5 
    if joystick_upos(i)<-0.5
    {
        con_type[argument0] = scc_type_axisneg;
         con_button[argument0]=8    
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 5 
    if joystick_upos(i)>0.5
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=9    
        con_controller[argument0]=i;
        return(1);
    }
    
    if joystick_axes(i) >= 6 
    if joystick_vpos(i)<-0.5
    {
        con_type[argument0] = scc_type_axisneg;
         con_button[argument0]=10      
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_axes(i) >= 6 
    if joystick_vpos(i)>0.5
    {
        con_type[argument0] = scc_type_axispos;
         con_button[argument0]=11       
        con_controller[argument0]=i;
        return(1);
    }
    if joystick_has_pov(i) 
    && (joystick_pov(i)==0
    || joystick_pov(i)==90
    ||joystick_pov(i)==180 
    ||joystick_pov(i)==270)
    {
        con_type[argument0] = scc_type_joystick_pov; //3
        var pov; //direction in degress just 0,45,90,135,180,225,270,315.. depends on joypad might have more maybe.
        pov = joystick_pov(i); 
        if pov == 0 { con_button[argument0]=0;}
        if pov == 90 { con_button[argument0]=90;}
        if pov == 180 { con_button[argument0]=180;}
        if pov == 270 { con_button[argument0]=270;}
        con_controller[argument0]=i;
        return(1);
    }
    if (i>25) return(0); //Glitch in Windows 8 were it keeps going in a endless loop if we don't do this.
}

return(0);
