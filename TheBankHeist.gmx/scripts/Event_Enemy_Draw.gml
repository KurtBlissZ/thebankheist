/// Event_Enemy_Draw()
if distance_to_object(obj_camera) > global.draw_dist exit;

draw_text(x - 30, y - 30, delay_spot);

//draw_text(x-100,y-40,script_get_name(path_state));
//Draw dist to detect player
//draw_set_color(c_green);
//draw_circle(x,y,detect_dist,1);
//draw_set_color(c_red);
//draw_circle(x,y,stop_dist,1);
//draw_set_colour(c_yellow);
//draw_circle(x,y,backup_dist,1);
//draw_set_color(c_red);
//draw_circle(x,y,fire_dist,1);
//Draw feild of vision
//draw_line(x,y,x+lengthdir_x(detect_dist,aim-fov/2),y+lengthdir_y(detect_dist,aim-fov/2))
//draw_line(x,y,x+lengthdir_x(detect_dist,aim+fov/2),y+lengthdir_y(detect_dist,aim+fov/2))
//draw_set_color(c_white);

//Draw healthbar
//if hp<100
//draw_healthbar(x-32,y-42,x+32,y-34,hp,c_black,c_red,c_green,0,1,1);

//Shawdow
d3d_transform_set_translation(0, 0, z-1);
draw_sprite_ext(sprite_index,image_index,x+5,y+5,image_xscale*1,image_yscale*1,aim,c_black,0.6);
d3d_transform_set_identity();

//Actor
d3d_transform_set_translation(0, 0, z);
draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,aim,image_blend,image_alpha);
d3d_transform_set_identity();


