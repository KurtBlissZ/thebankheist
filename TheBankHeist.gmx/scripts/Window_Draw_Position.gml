/// Window_Draw_Position()
if mouse_in_rectangle(x, y, bbox_right, y+18) 
&& mcp(mb_left) && click && active {
    draging = true;
    drag_x = x - mouse_x;
    drag_y = y - mouse_y;
    click = false;
} else if !mc(mb_left) {
    draging = false;
}

if draging {
    xpos = mouse_x + drag_x - view_xview;
    ypos = mouse_y + drag_y - view_yview;
}

x = clamp(xpos + view_xview, view_xview + 0, view_xview + view_wview - image_xscale);
y = clamp(ypos + view_yview, view_yview + 18, view_yview + view_hview - image_yscale);



