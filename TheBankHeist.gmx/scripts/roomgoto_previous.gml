/// roomgoto_previous()
if room_exists(room_previous(room)) {
    room_goto_previous();
    return true;
}
