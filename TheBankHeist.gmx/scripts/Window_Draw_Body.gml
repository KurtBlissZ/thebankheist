/// Window_Draw_Body(x,y)
bodyX = argument0;
bodyY = argument1;

switch(name) {
    case "Pick Texture": 
    Window_Body_Pick_Texture(); 
    break;

    case "Texure Repeat":  
    break;
}


/*// Window_Draw_Body(x,y)
var argx = argument0;
var argy = argument1;
var xx = argument0;
var yy = argument1;

switch(name) {
    case "Pick Texture": {
        var i=0, t, size = 48; 
        while background_exists(i) {
        if (string_copy(background_get_name(i),1,4)=="tex_") {
            if mouse_in_rectangle(xx, yy, xx+size, yy+size) && check_lmb() {
                Obj_Editor.current_tex = i;
            } 
            
            var c = iff(Obj_Editor.current_tex==i, c_red, c_white);
            
            draw_background_stretched_ext(i, xx-offx, yy-offy, size, size, c, 1);
            if (xx+(size*2) <= bbox_right) {
                xx += size;
            } else if (yy+(size*2) <= bbox_bottom) {
                xx = argx;
                yy += size;
            } else {
                //if draw_text_button_off(bbox_right-20, bbox_bottom-20, "\/", 
                //  -1, -1, offx, offy) {
                //}
                //break;
            }
        }
        yy += 64;
        var _repeat = array(8, 16, 32, 64, 128, 256, 512);
        for(var i=0, _X=argx; i<array_length_1d(_repeat); i+=1) {
            var _str = string(_repeat[i]);
            if draw_text_button_off(_X, yy, _str, 
            -1, iff(Obj_Editor.current_tex_repeat==_repeat[i],c_blue,c_black), 
            offx, offy) {
                Obj_Editor.current_tex_repeat = _repeat[i];
            }
            _X += string_width(_str) + 4;
        } 
    } break;

    case "Texure Repeat":  {
        if do_init {
            inputactive = 0;
            do_init = false;
        }
        inputstring = iff(inputactive,keyboard_string,Obj_Editor.current_tex_repeat);
        var a = array(xx, yy, xx+64, yy+24);
        draw_rectangle_col(a[0], a[1], a[2], a[3], iff(inputactive, c_aqua, c_ltgray));
        if mouse_in_rectangle(a[0], a[1], a[2], a[3]) && active {
            if mcp(mb_left) {
                inputactive = true;
                keyboard_string = inputstring;
            }
        }
        if kcp(vk_enter) && active && inputactive {
            Obj_Editor.current_tex_repeat = real(keyboard_string);
            inputactive = false;
        }
    } break;
    
    i+=1;
    }
}
