/// Menu_Enums()
// Define menu all enums here

// Menus
enum MENU {
    // Menus
    SET_MAIN,     
    SET_OPTIONS,
    SET_PAUSE,
    SET_STORE,
    SET_ROOMS,
    SET_CHEATS,

    // Actions
    ACTION_EXIT,
    ACTION_FULLSCREEN,
    ACTION_RESTART,
    ACTION_VSYNC,
    ACTION_FXAA,
    ACTION_SMOOTHING,
    ACTION_SHOW_CIVILIANS,
    
    // Built in actions    
    ACTION_DEBUG,
    ACTION_PREVIOUS,
    ACTION_MENU,
    ACTION_ROOM,
    ACTION_SCRIPT,
    ACTION_NONE,
    ACTION_SKIP
}

// Menu ACTIONS
// MENU.ACTION_PREVIOUS 
    // This action goes back to the last menu, 
        //keep that in mind it won't go back twice so if you go to a menu then another menu be sure to just change the menu
// MENU.ACTION_SKIP
   // Used as non menu options, so they can be skiped over
// MENU.ACTION_MENU
   // Use this action to change the menu set
// MENU.ACTION_NONE
    // Make a menu option do nothing without getting an error, 
    // Use as place holder
