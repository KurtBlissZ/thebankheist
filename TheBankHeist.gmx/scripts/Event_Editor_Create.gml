/// Event_Controller_Create()
depth = -100;
Editor_Menu_Create();

// Options
current_object = Obj_Object;
current_tex = tex_wall;
current_tex_top = tex_black;
current_tex_repeat = 128;
current_tex_top_repeat = 128;
current_height = 128;

// Program Window
center = false; 
prevw = window_get_width();
prevh = window_get_height();

// Mouse
click = true;
click_editor = true;
click_gui = true;
mouse_x_prev = floor(mouse_x);
mouse_y_prev = floor(mouse_y);

// Mode(s)
can_select = true;
can_create = true;
can_delete = true;

// Windows (In game)
global.windows_list = ds_list_create();
with (Obj_Window) {
    ds_list_add(global.windows_list, id);
}

