/// Event_Menu_Step()
if !window_has_focus() alarm[0]=5;
if alarm[0]!=-1 exit;

if (menu_select == -10) || COMON exit; //If there is no menu options

var k_up     = keyboard_check_pressed(vk_up)
                || keyboard_check_pressed(ord("W"));
var k_down   = keyboard_check_pressed(vk_down)
                || keyboard_check_pressed(ord("S"));
var k_select = (keyboard_check_pressed(vk_enter) 
                || mouse_check_button_released(mb_left))
                || keyboard_check_pressed(vk_space);
var k_back   = keyboard_check_pressed(vk_backspace);

var g = 0; 
var c = 0;

do if gamepad_is_connected(g) { 
    c = (gamepad_axis_value  (g, gp_axislv) < -0.5);
    if gp_can_up[g] && c {
        k_up           = true;
        gp_can_up[g]   = false;
    } else {
        if !c gp_can_up[g] = true;
    }

    c = (gamepad_axis_value  (g, gp_axislv) >  0.5);
    if gp_can_down[g] && c {
        k_down         = true;
        gp_can_down[g] = false;
    } else {
        if !c gp_can_down[g] = true;
    }

    k_up     = k_up     ||  gamepad_button_check_pressed(g, gp_padu);
    k_down   = k_down   ||  gamepad_button_check_pressed(g, gp_padd);
    k_select = k_select ||  gamepad_button_check_pressed(g, gp_face1);
    g++;
} until (!gamepad_is_connected(g))

//Navigate
if(k_up)   do {
    menu_select -= 1;
    if(menu_select < 0) 
        menu_select = menu_count;
} until (menu_info_action[menu_select] <> MENU.ACTION_SKIP)

if(k_down) do {
    menu_select += 1;
    if(menu_select > menu_count) 
        menu_select = 0;
} until (menu_info_action[menu_select] <> MENU.ACTION_SKIP)

//Pick Menu Option
if(k_select) Menu_Exeucte_Selected();

//Go to Prevous Menu
if(k_back) 
&& (menu_info_action[menu_count] == MENU.ACTION_PREVIOUS)
{
    menu_select = menu_count;
    Menu_Exeucte_Selected();
}
