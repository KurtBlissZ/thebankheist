/// string_countdown(steps)
var steps = argument0;
var mins = 0;
var secs = 0;

//Get secs
while steps > 60 {secs+=1; steps-=60;}

//Get mins
while secs > 60 {mins+=1; secs-=60;}

if secs<10 secs='0'+string(secs);

//Return string
return(string(mins)+':'+string(secs));
