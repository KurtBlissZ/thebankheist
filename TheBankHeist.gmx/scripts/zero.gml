///zero(x1,x2,x3,...)
/*
    This script returns the number that is closest to zero, disregarding
    the complications brought on by negative numbers. 
*/

var i = 0, 
    smallest = 0, 
    values = 0;

for(i=0;i<argument_count;i += 1){
    values[i] = argument[i];  
}

for (i = 0; i < (array_length_1d( values )) ; i += 1){
   if (abs(values[i]) < abs(values[smallest])){
      smallest = i;
   }
}

return values[smallest];


