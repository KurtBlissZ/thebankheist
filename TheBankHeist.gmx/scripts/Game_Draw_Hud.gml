/// Game_Draw_Hud()
draw_set_font(fnt_default);
draw_set_color(c_white);
draw_text(0,0,fps);
draw_text(0,32,fps_real);
draw_sprite(spr_hud,0,0,0);
draw_set_font(fnt_hud);
var w = sprite_get_width(spr_hud_bar);
var h = sprite_get_height(spr_hud_bar);

// Health
draw_sprite_part(spr_hud_bar,0,0,0,w*(health/100),h,68,682);
draw_text(68+32,682,health);

// Armor
draw_sprite_part(spr_hud_bar,1,0,0,w*(global.armor/100),h,129,645);
draw_text(129+32,645,global.armor);

// Ammo
draw_sprite_part(spr_hud_bar,2,0,0,w*(global.gun_mag[global.gun_index]/global.gun_mag_size[global.gun_index]),h,1002,658);
draw_text(1002+32,658,string(global.gun_mag[global.gun_index]) + ' / ' + string(global.ammo[global.gun_ammo_type[global.gun_index]]));

// Dosh
w = sprite_get_width(spr_hud_bar_long);
h = sprite_get_height(spr_hud_bar_long);
draw_sprite_part(spr_hud_bar_long,2,0,0,w*(global.dosh/global.dosh_limit),h,448,704);

// Van Dosh
with (obj_player) if (distance_to_object(obj_van) < 32)
    draw_text_ext_transformed_colour(160, 140, string(global.dosh_van)+' Dosh', 
        4, 300, 5, 5, 10, c_green,c_lime,c_green,c_lime,1);

// Timer
draw_text(1184, 32, string_countdown(global.timer));

// Dynamite
draw_sprite_ext(spr_dynamite, 0, 1184-24, 132-20, 2, 2, 0, c_black, 0.8);
draw_text(1184, 132, global.dynamites);

// Reinforcements bar
w = sprite_get_width(spr_reinf_bar);
h = sprite_get_height(spr_reinf_bar);
draw_sprite_part(spr_reinf_bar,2,0,0,w*(global.reinf/global.reinf_limit),h,448,18);

Game_Draw_Switch_Weapons();

//Objectives HUD
global.current_frame = draw_sprite_animate(spr_objtv_line, min(global.current_frame, sprite_get_number(spr_objtv_line)-1), 0.5, 0, 80);
