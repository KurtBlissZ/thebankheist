/// Event_Window_Create()
name = "Pick Texture";

window_script = Window_Body_Textures;

do_init = true;

xpos = x;
ypos = y;

sprite_index = Spr_Window;

image_xscale = 300;
image_yscale = 300;

active = false;

draging = false;
drag_x = x - mouse_x;
drag_y = y - mouse_y;

ds_list_add(global.windows_list, id);

active_input_field = -1;
