/// instance_new([x], [y], obj)
if (argument_count==1) return instance_create(0, 0, argument[0]); 
else return instance_create(argument[0], argument[1], argument[2]); 
