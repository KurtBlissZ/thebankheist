/// draw_text_button_off(x, y, str, [highlight color], [text color], [offx], [offy])

var xx = argument[0];
var yy = argument[1];
var ss = argument[2];

if ((argument_count>3) && (argument[3]!=-1)) {
    var hc = argument[3];
} else {
    var hc = c_red;
}

if ((argument_count>4) && (argument[4]!=-1)) {
    var c  = argument[4];
} else {
    var c  = c_black; //draw_get_color();
}
    
if ((argument_count>5) && (argument[5]!=-1)) {
    var ox  = argument[5];
} else {
    var ox  = 0;
}
    
if ((argument_count>6) && (argument[6]!=-1)) {
    var oy  = argument[6];
} else {
    var oy  = 0;
}

var _click = false;

if point_in_rectangle(mouse_x, mouse_y, xx, yy, 
xx + string_width(ss), yy + string_height(ss)) {
    _click = mouse_check_button_released(mb_left);
    draw_set_color(hc);
} else {
    draw_set_color(c);
}

draw_text(xx-ox, yy-oy, ss);

return _click;

