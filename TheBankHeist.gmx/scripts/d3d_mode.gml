///d3d_mode(mode)
if argument0 {
    // initialize 3D
    d3d_start();
    d3d_set_hidden(true);
    d3d_set_lighting(false);
    d3d_set_culling(true);
    d3d_fog(false,c_black,10,300);
    // interpolate textures
    texture_set_interpolation(true);
    // alpha test
    draw_set_alpha_test(true);
    draw_set_alpha_test_ref_value(128);
    // smooth textures
    texture_set_interpolation(true);
} else {
    d3d_end();
    d3d_fog(false);
    draw_set_colour(c_black);
    draw_set_alpha_test(false);
} 
