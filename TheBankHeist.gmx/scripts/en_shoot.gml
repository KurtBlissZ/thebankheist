///en_shoot()
with instance_create(x, y, obj_enemy_bullet) {
    direction = other.aim;
    image_angle = direction;
    speed = 32;
    instance_create(x,y,obj_casing);
    var o = other.id;
    with (instance_create(x,y,obj_explosion)) {
        owner = o;
        image_angle = other.direction;
    }
}
