/// Game_Data_Load()
// Game_Data_Save()
var i = ds_map_secure_load("savedata");

// Player
health = ds_map_find_value(i, "health");
score = ds_map_find_value(i, "score");
lives = ds_map_find_value(i, "lives");
global.armor = ds_map_find_value(i, "global.armor");

// Dosh
global.dosh_van = ds_map_find_value(i, "global.dosh_van");
global.dosh_limit = ds_map_find_value(i, "global.dosh_limit");

// Inventory
global.dynamites = ds_map_find_value(i, "global.dynamites");
global.gun_index = ds_map_find_value(i, "global.gun_index");
global.gun_have = list_to_array(ds_map_find_value(i, "global.gun_have"));
global.ammo = list_to_array(ds_map_find_value(i, "global.ammo"));
global.gun_mag = list_to_array(ds_map_find_value(i, "global.gun_mag"));

// Destroy Ds Map 
ds_map_destroy(i);

