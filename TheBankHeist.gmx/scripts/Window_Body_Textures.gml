/// Window_Body_Textures(x, y)
var i=0, t, size = 48; 
var xx = argument0, argx = xx;
var yy = argument1, argy = yy;

while background_exists(i++)
if (string_copy(background_get_name(i),1,4)=="tex_") {
    if mouse_in_rectangle(xx, yy, xx+size, yy+size) 
    && check_lmb() {
        Obj_Editor.current_tex = i;
    } 
    
    var c = iff(Obj_Editor.current_tex==i, c_red, c_white);
    
    draw_background_stretched_ext(i, xx-offx, yy-offy, size, size, c, 1);
    
    if (xx+(size*2) <= bbox_right) {
        xx += size;
    } 
    
    else if (yy+(size*2) <= bbox_bottom) {
        xx = argx;
        yy += size;
    } 
    
    else {
        //if draw_text_button_off(bbox_right-20, bbox_bottom-20, "\/", 
        //  -1, -1, offx, offy) {
        //}
        break;
    }
}

yy += 64;

var _repeat = array(8, 16, 32, 64, 128, 256, 512);

for(var i=0, _X=argx; i<array_length_1d(_repeat); i+=1) {
    var _str = string(_repeat[i]);
    
    if draw_text_button_off(_X, yy, _str, 
    -1, iff(Obj_Editor.current_tex_repeat==_repeat[i],c_blue,c_black), 
    offx, offy) {
        Obj_Editor.current_tex_repeat = _repeat[i];
    }
    
    _X += string_width(_str) + 4;
}



