/// Event_Game_Room_Start()
current_frame=0;
draw_texture_flush();

if instance_exists(par_player) {
    global.ingame = true;
    instance_singleton_ext(obj_grid, obj_camera);
    alarm[1]=1; // Create obj_world
    global.timer = global.timer_start;
    
    //application_surface_draw_enable(false);
    //Surf = surface_create(view_wview, view_hview);
    //view_surface_id[0] = Surf;
    
}

switch (room)
{
    case rm_title: 
    case rm_menu:
    if !audio_is_playing(bg_title) {
        audio_play_sound(bg_title,10,0);
        audio_sound_gain(bg_title,0.7,0);
        global.ingame = false;
    }   
    break;
    
    case rm_shop:
    global.selected = obj_sp_pistol;
    break;
}

