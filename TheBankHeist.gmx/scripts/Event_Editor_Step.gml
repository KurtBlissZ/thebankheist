/// Event_Controller_Step()
if !Menu_Selected
&& !instance_position(mouse_x,mouse_y,Obj_Window) {
    Editor_Edit();
}
 
/// Window Size
if window_has_focus()
&& (prevw!=window_get_width()) || (prevh!=window_get_height()) {
    view_wview=max(window_get_width(),1);
    view_wport=view_wview;
    view_hview=max(window_get_height(),1);
    view_hport=view_hview;
    surface_resize(application_surface,view_wview,view_hview);
    prevw=window_get_width();
    prevh=window_get_height();
}

/// Level Panning
if keyboard_check(vk_alt) {
    //init mouse change
    var vWinMouseX = window_mouse_get_x();
    var vWinMouseY = window_mouse_get_y();
    var vWinWidth = floor(window_get_width()*0.5);    //floor is needed to prevent camera moving by its self,
    var vWinHeight = floor(window_get_height()*0.5);              // when window is resized
    
    if keyboard_check_pressed(vk_alt) {
        mouse_x_save = display_mouse_get_x();
        mouse_y_save = display_mouse_get_y();
        window_set_cursor(cr_none);
        window_mouse_set(vWinWidth, vWinHeight);
    } else {
        //apply mouse movement
        view_xview += ((vWinMouseX - vWinWidth));
        view_yview += ((vWinMouseY - vWinHeight));    
        
        //set mouse position back
        window_mouse_set(vWinWidth, vWinHeight);
    }

} else if keyboard_check_released(vk_alt) {
    window_set_cursor(cr_default);
    display_mouse_set(mouse_x_save, mouse_y_save);    
}

// De Selecting Windows
if mcp(mb_left) 
with Obj_Window {
    active = false;
} 

// Selecting Window
for (var i=ds_list_size(global.windows_list)-1; i>=0; i-=1) {
    var inst_id = ds_list_find_value(global.windows_list, i);    
    with inst_id {
        if click && mcp(mb_left) && mouse_hover() {
            ds_list_delete(global.windows_list,i);
            ds_list_add(global.windows_list,id);
            click = false;
            active = true;
        }
    }
}


