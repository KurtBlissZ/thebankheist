///en_player_in_sight()
if instance_exists(obj_player) {
    return !collision_line(x,y,obj_player.x,obj_player.y,par_wall_shootable,0,1);
}
