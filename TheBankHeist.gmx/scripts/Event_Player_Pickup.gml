/// Event_Player_Pickup()
switch (other.object_index) {
    case obj_ammo:
        /// Gain 6 Pisol Bullets
        var _gi = global.gun_index;
        var _at = global.gun_ammo_type[_gi]
        if global.ammo[_at]>=6*8 exit;
        audio_play_sound(snd_ammo,10,0);
        global.ammo[_at]+=6;
        if global.ammo[_at]>6*8 global.ammo[_at]=6*8;
        destroy(other);
        break;
    
    case obj_armor:
        ///Gain 25 armor
        if (global.armor>=100) exit;
        audio_play_sound(snd_armor,10,0);
        global.armor+=25;
        if global.armor>100 global.armor=100;
        destroy(other);
        break;
    
    case obj_health:
        ///Gain 25 heatlh
        if health>=100 exit;        
        audio_play_sound(snd_mediket,10,0);
        health = min(100, health+25);
        destroy(other);
        break;
}
