/// Event_Game_Create()
ini_open("prefs.ini"); //Game_Preferences_Save();

gml_release_mode(ini_read_real("Game","gml_release_mode",false));

// Game
global.ingame = false;
global.Show_Civilians = ini_read_real("Game","Show_Civilians",true);
global.Delta_Enabled = ini_read_real("Game","Delta_Enabled",true);
global.Delta = 1;
Game_Create_Inventory();

// Debuging
debug_enabled = true;
last_command = "help";
log = "";
logmsg = "";
enter_command = false;

// Display
display_set_gui_size(1280, 720);
global.switching = false;
global.draw_objv = false;
global.current_frame = 0; //for objetive bar in hud

Scr_Interpolation(ini_read_real("Display","Interpolation",false));
global.gAA = ini_read_real("Display","AA",0);
global.gVsync = ini_read_real("Display","VSYNC",false);
display_reset(global.gAA, global.gVsync);

global.Can_Fullscreen = (global.gAA==0); // Needs to be after AA Init ;p

if ini_read_real("Display","Fullscreen",true) 
&& global.Can_Fullscreen {
    window_set_fullscreen(true);
}

// Audio
audio_group_load(audiogroup_title);
global.AudioGain = ini_read_real("Audio","MasterGain",1);
audio_master_gain(global.AudioGain);

// Http
filename = "updater.ini";
file = http_get_file("http://tiny.cc/tbhupdater", filename); 

//Player's 
health = 100;
score = 0;
lives = 3;
global.armor = 0;

// Init Gamepad 
global.gamepad          = false;
global.gamepad_device   = 0;
global.GP_ls_can_up     = true;
global.GP_ls_can_down   = true;

// Reinforcents
global.reinf                = 0;
global.reinf_limit          = 3000;
global.call_reinf           = false;
global.play_police_siren    = false;
global.reinf_per            = true;
global.timer                = -1;
global.timer_start          = 60*60*2;

//Dosh / Cash
global.dosh         = 0;
global.dosh_limit   = 9000;
global.dosh_van     = 10000;

//Draw Distance
global.draw_dist = 416; 

//Pause
global.pause        = false;
global.pause_surf   = -1;

// Set mouse back after switching weapons
mousex_setback = 0;
mousey_setback = 0;

// Shader
Shaders_Test();

uni_brightness_amount = shader_get_uniform(shd_bright_contrast,"brightness_amount");
var_brightness_amount = ini_read_real("Display","Brightness",0);

uni_contrast_amount = shader_get_uniform(shd_bright_contrast,"contrast_amount");
var_contrast_amount = ini_read_real("Display","Contrast",0);

// Close prefs.ini
ini_close();

// Init Custom Controls
if file_exists("controls.ini") {
    instance_singleton(obj_scc); // Custom Controls Obj
}

// Go to next room
alarm[0] = 15;

