/// Editor_Merge_Walls()
var arg0 = Obj_Object_Wall;
for (var i=0; i<=instance_number(arg0); i+=1)
with instance_find(arg0, i) {
    var c = false; 
    
    // Merge Walls Hor
    for (var j=1; j<=image_yscale; j+=1) {
        with instance_position(bbox_right+16, y + (j*32) - 16, arg0) 
        if (bbox_top == other.bbox_top) && (bbox_bottom == other.bbox_bottom)
        && Editor_Matching_Walls() {  
            other.image_xscale += image_xscale;
            instance_destroy();
            i=0;
            c=true;
            break; 
        } 
        
        with instance_position(bbox_left-16, y + (j*32) - 16, arg0) 
        if (bbox_top == other.bbox_top) && (bbox_bottom == other.bbox_bottom) 
        && Editor_Matching_Walls() {
            other.image_xscale += image_xscale;
            other.x -= sprite_width;
            instance_destroy();
            i=0;
            c=true;
            break; 
        }  
    } if c continue;
    
    // Merge Walls Ver
    for (var j=1; j<=image_xscale; j+=1) {
        with instance_position(x + (j*32) - 16, bbox_bottom + 16, arg0)
        if (bbox_left == other.bbox_left) && (bbox_right == other.bbox_right) 
        && Editor_Matching_Walls() {
            other.image_yscale += image_yscale;
            instance_destroy();
            i=0;
            break; 
        } 
        
        with instance_position(x + (j*32) - 16, bbox_top - 16, arg0)
        if (bbox_left == other.bbox_left) && (bbox_right == other.bbox_right) 
        && Editor_Matching_Walls() {
            other.image_yscale += image_yscale;
            other.y -= sprite_height;
            instance_destroy();
            i=0; 
        } 
    } 
}

