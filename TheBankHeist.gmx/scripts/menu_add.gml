#define Menu_Add
/// Menu_Add(name, action, sub)

// Adds a menu options

// Optional Arguments
if(argument_count>2) var arg_sub = argument[2];
else                 var arg_sub = -1;

// Inscrease menu items
menu_count += 1;

// Menu item information
menu_info_name   [menu_count] = argument[0];
menu_info_action [menu_count] = argument[1];
menu_info_sub    [menu_count] = arg_sub;


#define Menu_Add_Action
/// Menu_Add_Action(name, action)

// adds an menu option with a menu action

Menu_Add(argument0, argument1);


#define Menu_Add_Menu
/// Menu_Add_Menu(name, menu)

// This menu options changes your menu

Menu_Add(argument0, MENU.ACTION_MENU, argument1);



#define Menu_Add_Room
/// Menu_Add_Room(name, room)

// This menu option sends you to a room

Menu_Add(argument0, MENU.ACTION_ROOM, argument1);




#define Menu_Add_Script
/// Menu_Add_Script(name, script)

// This menu option executes a script

Menu_Add(argument0, MENU.ACTION_SCRIPT, argument1);



#define Menu_Add_Text
/// Menu_Add_Text(string)

// This adds text to the menu, use it for menu titles or catagories

Menu_Add(argument0, MENU.ACTION_SKIP);


#define Menu_Add_Sepperator
/// Menu_Add_Sepperator()

// adds a space in the menu

Menu_Add('', MENU.ACTION_SKIP);
#define Menu_Add_Debug
/// Menu_Add_Debug(name, command)
Menu_Add(argument0, MENU.ACTION_DEBUG, argument1);