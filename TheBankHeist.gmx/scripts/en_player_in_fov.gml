///en_player_in_fov()
if instance_exists(obj_player) {
    var dir_to_player = point_direction(x,y,obj_player.x,obj_player.y);
    return (abs(angle_difference(aim, dir_to_player)) < fov/2);
}
