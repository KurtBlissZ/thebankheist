/// destroy([obj])
if (argument_count==0)   instance_destroy();
else with (argument[0])  instance_destroy();
