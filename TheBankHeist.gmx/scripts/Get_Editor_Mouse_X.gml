/// Get_Editor_Mouse_X()

if instance_exists(Obj_Editor_Camera) {
    return Obj_Editor_Camera.__x;
}

return mouse_x;

