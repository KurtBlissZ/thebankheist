#define scr_player_minigame
/// scr_player_minigame()
minigame_x += 1 * minigame_dir;

if minigame_x>minigame_x2 {
    minigame_x = minigame_x2;
    minigame_dir = -1;
} 

else if minigame_x<0 {
    minigame_x = minigame_x2;
    minigame_dir = 1;
}


#define scr_player_minigame_init
/// scr_player_minigame_init()
state = player.state_minigame;

minigame_x = 0;
minigame_x1 = 0;
minigame_x2 = 128;
minigame_dir = 1; // -1 left, 1 right

minigame_goal = random(minigame_x2);

minigame_timer = 60;


#define scr_player_minigame_draw
/// scr_player_minigame_draw(xx, yy)
var x1 = argument0;
var y1 = argument1;
var x2 = x1 + minigame_x2;
var y2 = y1 + 32;

// back
draw_set_color(c_white);
draw_rectangle(x1, y1, x2, y2, false);

// goal
var m_x  = minigame_goal;
var m_y  = y1 + ((y1+y2)/2);
var ss = 12;
draw_set_color(c_green);
draw_rectangle(mx_x-ss, m_y-ss, m_x+ss, m_y+ss, 0);

// cursor 
m_x = minigame_x;
m_y = y1 + ((y1+y2)/2);
draw_set_color(c_blue);
draw_set_alpha(0.7);
draw_rectangle(mx_x-ss, m_y-ss, m_x+ss, m_y+ss, 0);
draw_set_alpha(1);