/// Event_Object_Draw()

if tex==-1 {
    draw_self();
} else {
    
    if !instance_exists(Obj_Editor_Camera) {
        draw_background_stretched_ext(tex, x, y, 
          sprite_width, sprite_height, image_blend, 1);
        
        if object_index==Obj_Object_Wall {  
            draw_set_alpha(0.4);
            draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, 0);
            draw_set_alpha(1);
        }
    }
    //draw_self();
    
}


