///Interact / Dosh / Van / Doors
if instance_exists(obj_scc) {
    var pressed_space = (obj_scc.check_button_pressed[obj_scc.con_action]
                        || obj_scc.check_button_pressed[obj_scc.con2_action])
                        && COMOFF;
    var pressed_control = (obj_scc.check_button_pressed[obj_scc.con_action2]
                        || obj_scc.check_button_pressed[obj_scc.con2_action2])
                        && COMOFF;   
} else {
    var pressed_space = (keyboard_check_pressed(vk_space) || GP_b_presseed())&&COMOFF;
    var pressed_control = keyboard_check_pressed(vk_control)&&COMOFF;
}
interact = noone;

//Steal Dosh From Vault / Lockers
if Player_Interact(obj_vault) {    
    if pressed_space
    if (interact.dosh > 0)
    if global.dosh < global.dosh_limit {
        audio_play_sound(snd_money,10,0);
        instance_create(x,y,obj_dosh);
        instance_create(x,y,obj_score);
        var xx = min(100, interact.dosh);
        interact.dosh-=xx;
        global.dosh+=xx;
    } if (sprite_index<>spr_char0_stealing) 
    if (interact.dosh > 0)
    if (global.dosh < global.dosh_limit) {
        sprite_index = spr_char0_stealing;
    } alarm[2] = 1;    
    if (interact.dosh == 0) interact = noone;
}

//Interaction with Van
if Player_Interact(obj_van) {
    if pressed_space {//Put dosh in van
        audio_play_sound(snd_money,10,0);
        global.dosh_van += global.dosh;
        global.dosh = 0;
    } if pressed_control {//Get in van
        /*var i = instance_create(interact.x,interact.y,obj_player_van);
        i.image_angle = interact.image_angle;
        i.direction = interact.image_angle;
        with interact instance_destroy();
        instance_destroy();*/
        
        // make your exscape:
        
        
    } if (global.dosh <= 0) interact = noone;
} 

//Open door
if Player_Interact(obj_door) {
    if (interact.state==0) {
        if pressed_space
            with interact event_user(0);
        sprite_index = spr_char0_stealing;
    } alarm[2] = 1;
    if (interact.state<>0) interact = noone;
}

// Exit
if Player_Interact(obj_exit_mission){
    if pressed_space
        room_goto(rm_mission_complete);
    
    with interact {
        //image_blend=c_red;
        
    }
}


