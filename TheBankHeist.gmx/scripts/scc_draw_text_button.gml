///scr_draw_text_button(x,y,string);
var xx,yy,str,endx,endy,return_color,_click;
xx = argument0;
yy = argument1;
str = argument2;
endx = xx+string_width(str);
endy = yy+string_height(str);
return_color = draw_get_color();
_click = 0;
if mouse_x>xx && mouse_x<endx && mouse_y>yy && mouse_y<endy {
    draw_set_color(c_red);    
    if mouse_check_button_pressed(mb_left) _click = 1;
} else {
    draw_set_color(c_black);
}                                                                               
draw_text(xx,yy,str);
draw_set_color(return_color);
return(_click);
