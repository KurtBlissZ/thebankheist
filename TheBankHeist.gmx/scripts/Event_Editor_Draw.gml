/// Event_Controller_Draw()
var __dgat = draw_get_alpha_test();
if instance_exists(Obj_Editor_Camera) {
    d3d_set_projection_ortho(0,0,view_wview,view_hview,0);
    d3d_set_hidden(false);   
    draw_set_alpha_test(false);
    var offx = view_xview;
    var offy = view_yview;
} else {
    var offx = 0;
    var offy = 0;
}

// Draw Windows
for (var i=0,c=true; i<ds_list_size(global.windows_list); i+=1) {
    var inst_id = ds_list_find_value(global.windows_list, i);
    with inst_id {
        Window_Draw(offx, offy);
    }
}


// Draw Tool Bar
var starty = view_yview-offy;
var hh = menubarheight;
var xx = view_xview+10-offx;
var yy = starty;

draw_rectangle_col(view_xview-offx,view_yview-offy,
  view_xview+view_wview-offx,view_yview+hh-offy,c_ltgray);

for (var i=0; i<array_length_1d(Menu_Cat); i++) {

    // Category
    var s = Menu_Cat[i];
    if mouse_in_rectangle(xx+offx, yy+offy, xx+string_width(s)+offx, yy+hh+offy) {
        draw_set_color(c_blue);
        if check_lmb() { 
            Menu_Selected=iff((Menu_Selected==i), -1, i);            
        } else if (Menu_Selected>-1) {
            Menu_Selected = i;    
        }
    } else {
        draw_set_color(c_black);
    }
    draw_text(xx, yy, s);
    
    // Buttons 
    if (Menu_Selected == i) {
        butlen = array_length_2d(Menu_Button,i);
        draw_rectangle_col(xx, yy+hh, xx+Cat_Width[i], yy+hh+hh*butlen, c_ltgray);
        for (var a=0; a<butlen; a+=1) {
            yy+=hh;
            var d = Menu_Button[i,a];
            if mouse_in_rectangle(xx+offx,yy+offy,xx+Cat_Width[i]+offx,yy+hh+offy) {
                draw_set_color(c_blue);
                if check_lmb(script_exists(Menu_Button_Action[i,a])) {
                    script_execute(Menu_Button_Action[i,a],Menu_Button_Arg[i,a]);
                    Menu_Selected = -1;
                    click = -5;
                }
            } else {
                draw_set_color(c_black);
            }
            draw_text(xx, yy, d);
        }
    }
    
    // Move position over
    xx+=string_width(s)+catsep;
    yy=starty;
}

draw_set_color(c_black);
draw_text_allign(view_xview+view_wview-10-offx,view_yview-offy,object_get_name(current_object));


if check_lmb() {
    Menu_Selected = -1;
}


if instance_exists(Obj_Editor_Camera) {
    draw_set_alpha_test(__dgat);
    d3d_set_hidden(true);
}

