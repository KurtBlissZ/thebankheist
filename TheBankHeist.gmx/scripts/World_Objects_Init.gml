#define World_Objects_Init
/// World_Objects_Init()
// textures used for models must have the prefix tex_ !
// DTS = DrawTypeStatic (default) // DTD = DrawTypeDynamic (used for non models, like doors)
switch (object_index)
{
    // Floors
    case obj_floor:      World_Init_Floor(tex_floor);break;
    case obj_floor_tile: World_Init_Floor(tex_floor3);break;
    case obj_road:       World_Init_Floor(tex_road, DTS, 128, 128);break;
    case obj_grass:      World_Init_Floor(tex_grass, DTS, 128, 128);break;        
    // Walls
    case obj_wall:       World_Init_Wall(tex_wall);break;
    case obj_factory:    World_Init_Wall(tex_factory, DTS, 256);break;
    case obj_table:      World_Init_Wall(tex_wall_wood, DTS, 24, Default, Default, Default, tex_wall_wood);break;
    case obj_wall_tile:  World_Init_Wall(tex_wall_tile);break;
    case obj_wall_wood:  World_Init_Wall(tex_wall_wood);break;
    case obj_wall_brick: World_Init_Wall(tex_wall, DTS, 64, Default, Default, Default, tex_wall);break;
    // Dynamic Walls
    case obj_door:       World_Init_Wall(texture_door, DTD, 127, 64, 64, Default, tex_white);break;
    case obj_vault:      World_Init_Wall(texture_vault, DTD, 64, 64, 64, Default, texture_locker);break;
    case obj_locker:     World_Init_Wall(texture_locker, DTD, Default, 64, 64, Default, texture_locker);break;
    case obj_wall_exp:   World_Init_Wall(tex_wall, DTD, Default, Default, Default, Default, tex_wall);break;
    // Buildings
    case obj_building1:  World_Init_Wall(tex_building,  DTS, 256);break;
    case obj_building2:  World_Init_Wall(tex_building2, DTS, 382);break;
    case obj_building3:  World_Init_Wall(tex_building3, DTS, 201);break;
    case obj_building4:  World_Init_Wall(tex_building4, DTS, 426);break;
    
    default: 
    DrawTypeIndex = DrawTypeOther;
    //if object_is_ancestor(object_index, par_car) {
    //    DrawTypeIndex = DrawTypeOther;
    //} else 
    //show_error("World Object ["+object_get_name(object_index)+"] Not Init in World_Object_Init!", true);
}


#define World_Init_Floor
/// World_Init_Floor(tex, [DrawType], [hrp], [vrp], [z])
if (argument_count>0) && !(argument[0]==Default)
&& background_exists(argument[0])
      Tex    = argument[0];
else  Tex    = tex_black;

if (argument_count>1) && !(argument[1]==Default)
                      DrawTypeIndex = argument[1];
else                  DrawTypeIndex = DrawTypeStatic;

if (argument_count>2) && !(argument[2]==Default)
                      Hrp = argument[2];
else                  Hrp = 256;

if (argument_count>3) && !(argument[3]==Default)
                      Vrp = argument[3];
else                  Vrp = 256;

if (argument_count>4) && !(argument[4]==Default)
                      Z   = argument[4];
else                  Z   = -1;


#define World_Init_Wall
/// World_Init_Wall(tex, [DrawType], [ztop], [hrp], [vrp], [zbottom], [toptex])
if (argument_count>0) && (argument[0]!=Default)
&& background_exists(argument[0])
      Tex    = argument[0];
else  Tex    = tex_white;

if (argument_count>1) && (argument[1]!=Default) 
                      DrawTypeIndex = argument[1];
else                  DrawTypeIndex = DrawTypeStatic;

if (argument_count>2) && (argument[2]!=Default) 
                      Z2  = argument[2];
else                  Z2  = 128;

if (argument_count>3) && (argument[3]!=Default) 
                      Hrp = argument[3];
else                  Hrp = 256;

if (argument_count>4) && (argument[4]!=Default)
                      Vrp = argument[4];
else                  Vrp = 256;

if (argument_count>5) && (argument[5]!=Default)
                      Z1  = argument[5];
else                  Z1  = 0;

if (argument_count>6) && (argument[6]!=Default)
&& background_exists(argument[6])
      TexTop = argument[6];
else  TexTop = tex_black;