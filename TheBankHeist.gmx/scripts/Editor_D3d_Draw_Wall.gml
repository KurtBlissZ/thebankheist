/// Editor_D3d_Draw_Wall(height, tex, [tex repeat], [tex top], [tex top repeat])

if ((argument_count>0) && (argument[0]!=-1)) {
    var _height = argument[0];
} else {
    var _height = 128;
}

if ((argument_count>1) && (argument[1]!=-1)) {
    var _tex = argument[1];
} else {
    var _tex = tex_wall;
}

if ((argument_count>2) && (argument[2]!=-1)) {
    var _texrp = argument[2];
} else {
    var _texrp = 128;
}

if ((argument_count>3) && (argument[3]!=-1)) {
    var _textop = argument[3];
} else {
    var _textop = tex_black;
}

if ((argument_count>4) && (argument[4]!=-1)) {
    var _textoprp = argument[4];
} else {
    var _textoprp = 128;
}

var i = background_get_texture(_tex),
    ii= background_get_texture(_textop);

d3d_draw_floor(x, y+sprite_height, _height, 
x+sprite_width, y, _height, ii, sprite_width/_textoprp,sprite_height/_textoprp);

d3d_draw_wall(x+sprite_width,y+sprite_height,0,
x,y+sprite_height,_height, i,sprite_width/_texrp,_height/_texrp);

d3d_draw_wall(x,y,0,
x+sprite_width,y,_height, i, sprite_width/_texrp,_height/_texrp);

d3d_draw_wall(x+sprite_width,y,0,
x+sprite_width,y+sprite_height, _height, i, sprite_height/_texrp,_height/_texrp);

d3d_draw_wall(x,y+sprite_height,0,
x,y,_height, i, sprite_height/_texrp,_height/_texrp);

