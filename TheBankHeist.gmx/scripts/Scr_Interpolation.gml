///Scr_Interpolation([linear])
if (argument_count>0) {
    global.linear = argument[0];
}
else{
    global.linear = !global.linear;
}
texture_set_interpolation(global.linear);
