///Player_Interact(obj)
//for obj_player
if instance_exists(argument0) {
    var col = collision_line(x,y,x+lengthdir_x(30,aim),y+lengthdir_y(30,aim),argument0,0,0);
    if col interact = col;
    return(col);
} 
