/// Event_Game_Step_Begin()

/// Toggle Gamepad / GP_up&down
if keyboard_check(vk_anykey) || mouse_check_button(mb_any) global.gamepad = false;

if gamepad_is_supported()
for (i=0; i<=gamepad_get_device_count(); i+=1)
{
    if gamepad_button_check(i,gp_start)
    || gamepad_button_check(i,gp_face1) {
        global.gamepad = true;
        global.gamepad_device = i;
        //gamepad_set_axis_deadzone(i, 0.4);
        break;
    }
};

//Tell when your have pressed up or down with analog stick
if global.gamepad {
    if !GPany_leftStrick_up() global.GP_ls_can_up = true;
    if !GPany_leftStrick_down() global.GP_ls_can_down = true;
}


