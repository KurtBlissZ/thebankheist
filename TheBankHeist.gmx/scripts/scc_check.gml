var n = argument0;

scc_check_keyboard(n);

if !check_gamepad_delay-- {
    
    check_gamepad_delay = room_speed * 3;
    
    has_gamepad = false;
    
    var gp_num = gamepad_get_device_count();
    for (var i = 0; i < gp_num; i++;) {
        var gic = gamepad_is_connected(i) 
        if gic {
            has_gamepad = true; 
            check_gamepad_delay = room_speed * 15;
            break;
        } 
    }
    
}


if has_gamepad {
    scc_check_gamepad(n);
} 

//scc_check_joystick(n);
