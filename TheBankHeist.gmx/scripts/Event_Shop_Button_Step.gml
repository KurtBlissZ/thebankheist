/// Event_Shop_Button_Step()
if point_in_rectangle(mouse_x, mouse_y,
bbox_left, bbox_top, bbox_right, bbox_bottom) 
{
    grow = min(grow+grow_rate, grow_max);
    Shop_Hover_Action(object_index);
    
    if mouse_check_button_pressed(mb_left)
        Shop_Purchase_Item(object_index);
}
 
else 
{
    grow = max(grow-grow_rate, grow_min);
    Shop_Nohover_Action(object_index);
}

xscale = grow; 
yscale = grow;



