/// Game_Draw_Switch_Weapons()

// Get Control
if instance_exists(obj_scc) with obj_scc {
    // Keyboard
    global.switching = check_button[con_switch]; //|| check_button[con2_switch];        
} else {
    // Keyboard
    global.switching = keyboard_check(vk_tab);
}

// Switching Weapons With Mouse
if global.switching {
    
    //Draw weapons
    var c = c_black;
    for(i=0; i<=global.gun_last; i++) {   
        var xx = 377+(i*246)-128;
        var yy = 200;
        
        var sw = sprite_get_width(spr_guns_hud);
        var sh = sprite_get_height(spr_guns_hud);
        
        if global.gun_have[i]
            if point_in_rectangle(mouse_x-view_xview[0],
            mouse_y-view_yview[0],xx,yy,xx+sw,yy+sh)
                global.gun_index = i;
        
        
        if (global.gun_index == i) c = c_red; else c = c_white;
        if !global.gun_have[i] c = c_gray;
        draw_sprite_ext(spr_guns_hud, global.gun_hud[i], xx, yy,
            1,1,0,c,1);
    }
    
    //Set player the sprite
    if instance_exists(obj_player)
        obj_player.sprite_index = global.gun_sprite[global.gun_index];
}
