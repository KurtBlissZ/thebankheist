/// Window_Body_Object_Properties(x, y)
var xx = argument0, argx = xx;
var yy = argument1, argy = yy;

draw_set_color(c_black);

xx += 10; 
yy += 10;

draw_text(xx, yy, oid);

var sh = ceil(string_height("|")*1.5);

yy += sh;
//draw_text(xx, 
Draw_Window_Text_Field(xx, yy, IF.X);

yy += sh;
Draw_Window_Text_Field(xx, yy, IF.Y);

yy += sh * 2;

if  draw_text_button(xx, yy, "apply changes", c_blue, c_black)
&& active {
    // Update changes
    oid.x = input_field[IF.X];
    oid.y = input_field[IF.Y];
}

yy += sh;

if draw_text_button(xx, yy, "jump to object", c_blue, c_black)
&& active {
    view_xview = oid.x - view_wview * 0.5;
    view_yview = oid.y - view_hview * 0.5;
}

switch (object_index)
{
    case Obj_Object_Floor: {
        
    } break;
    
    case Obj_Object_Wall: {
        
    } break;
}


/***************************************************
  Init for objects:
  
  Event_Object_Create
  Editor_Edit
 ***************************************************/

