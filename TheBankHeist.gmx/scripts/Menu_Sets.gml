#define Menu_Sets
/// Menu_Sets(menu)
Menu_Sets_Init(argument0);

// Menus
switch(current_menu)
{
    case MENU.SET_MAIN:
    Menu_Add_Room("Start Game", rm_shop); // You can replace room with whatever you want
    Menu_Add_Room("How To Play", rm_how_to_play);
    Menu_Add_Menu("Change Room", MENU.SET_ROOMS);
    Menu_Add_Menu("Cheats", MENU.SET_CHEATS);
    Menu_Add_Room("Level Editor", rm_editor);
    Menu_Add_Menu("Options", MENU.SET_OPTIONS);
    Menu_Add_Script("Save", Game_Data_Save);
    Menu_Add_Script("Load", Game_Data_Load);
    Menu_Add_Action("Exit Game", MENU.ACTION_EXIT);
    repeat(6)Menu_Add_Sepperator();
    Menu_Add_Text(" Version "+string(GM_version));
    Menu_Add_Text('Build '+date_date_string(GM_build_date));
    break;

    case MENU.SET_PAUSE:
    Menu_Add_Script("Continue", scr_menu_pause);
    Menu_Add_Menu("Change Room", MENU.SET_ROOMS);
    Menu_Add_Menu("Cheats", MENU.SET_CHEATS);
    Menu_Add_Menu("Options", MENU.SET_OPTIONS);
    Menu_Add_Script("Save", Game_Data_Save);
    Menu_Add_Action("Exit To Tiled Screen", MENU.ACTION_RESTART);
    Menu_Add_Action("Exit To Desktop", MENU.ACTION_EXIT);
    break;
    
    case MENU.SET_ROOMS:
    Menu_Add_Action("Back", MENU.ACTION_PREVIOUS);
    var i=0; while(i<=room_last) {Menu_Add_Room(room_get_name(i), i); i++;}
    break;
    
    case MENU.SET_CHEATS:
    Menu_Add_Debug("woohoo", "woohoo");    
    Menu_Add_Debug("health=100", "health=100");
    Menu_Add_Debug("armor=100", "armor=100");
    Menu_Add_Debug("ammo=100", "ammo=100");
    Menu_Add_Action("Back", MENU.ACTION_PREVIOUS);
    break;
    
    case MENU.SET_OPTIONS:
    Menu_Add_Text("OPTIONS");
    if global.Can_Fullscreen
    Menu_Add_Action("Fullscreen", MENU.ACTION_FULLSCREEN);
    Menu_Add_Action("Vsync ["+iff(global.gVsync, 'True', 'False')+"]", MENU.ACTION_VSYNC);
    Menu_Add_Action("FXAA ["+iff(!global.gAA, 'Off', string(global.gAA))+"]", MENU.ACTION_FXAA);
    Menu_Add_Action("Smoothing ["+iff(!global.linear, 'Off', string(global.linear))+"]", MENU.ACTION_SMOOTHING);
    Menu_Add_Action("Show Civilians ["+iff(global.Show_Civilians, "True", "False")+"]", MENU.ACTION_SHOW_CIVILIANS);
    Menu_Add_Room("Change Controls", rm_custom_controls);
    Menu_Add_Action("Back", MENU.ACTION_PREVIOUS);
    break;
    
    default: show_error("Menu ["+string(argument0)+"] doesn't exists ", false);
}

// Skip over the non menu item options
while (menu_info_action[menu_select] == MENU.ACTION_SKIP) {
    menu_select = min(menu_select+1 ,menu_count+1);

    // Prevents getting stuck in loop
    if(menu_select > menu_count) {
        show_error('Menu[' +string(menu_select)+ '] has no menu options', false);
        menu_select = -10;  //TO avoid you navigating menu
        break;
    }
}

/***************************************************
    Menu_Add(name, action, sub)
    Menu_Add_Menu(name, menu)
    Menu_Add_Action(name, action)
    Menu_Add_Room(name, room)
    Menu_Add_Script(name, script)
    Menu_Add_Text(text)
    Menu_Add_Sepperator()
 ***************************************************/




#define Menu_Sets_Init
/// Menu_Sets_init()
menu_use_mouse  =  0; // Switches be its self
menu_select     =  0; // Current menu item you are on
menu_count      = -1; // Ammount of menu items
select_previous = menu_select;
set_previous    = current_menu; // Keep track of last menu so you can go back
current_menu    = argument0;

// Options
menu_color        = c_white;
menu_color_select = c_red;
menu_font         = fnt_menu;
menu_font_select  = fnt_menu_selected;