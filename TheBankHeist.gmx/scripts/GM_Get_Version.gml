#define GM_Get_Version
/// GM_Get_Version(GM_Version, type 1/2/3/4)
// Return part of a version number with three decimals like 1.0.0.0 as a real number
var i=0,t=1,last=0;

while ( i <= string_length(argument0) ) {
    i++;
    
    if i==string_length(argument0) {
        if (argument1==t)
            return real( string_copy(argument0, last+1, abs((i)-last)) ); 
    }
    
    else if string_char_at(argument0,  i) == "." {
        if (argument1==t)return 
            real( string_copy(argument0, last+1, abs((i-1)-last))); 
        last = i;  
        t++;
    }
}

#define GM_Version_Check
/// GM_Version_Check(current, newest)

if GM_Get_Version(argument0, 1) < GM_Get_Version(argument1, 1)
    return true;
    
else if GM_Get_Version(argument0, 2) < GM_Get_Version(argument1, 2)
    return true;

else if GM_Get_Version(argument0, 3) < GM_Get_Version(argument1, 3)
    return true;

else if GM_Get_Version(argument0, 4) < GM_Get_Version(argument1, 4)
    return true;