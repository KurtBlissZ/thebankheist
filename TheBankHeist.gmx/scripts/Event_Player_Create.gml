/// Event_Player_Create()
interact = noone;
delta = 1;
hostile = true;

// Sprite / Depth
sprite_index = global.gun_sprite[global.gun_index];
z = 32;
img_spd = image_speed;
image_speed = 0;

// Sprite Size
image_xscale = 0.8; 
image_yscale = 0.8;

// Players State
enum player {
    state_normal,
    state_minigame
}
state = player.state_normal;

// Running
run_scale = 0;
run_scale_spd = 0.5;
running = false;

// Shooting
aim = 0;
can_shoot = true;
can_reload = true;

//timers
alarm_shoot_delay = 0;
alarm_reloading = 0;
alarm_set_color_back = 0;
delay_footstep = 0;

//timer
time_limit = 60*60*2;

