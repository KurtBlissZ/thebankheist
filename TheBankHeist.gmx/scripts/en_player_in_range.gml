///en_player_in_range()
if instance_exists(obj_player)
&& distance_to_object(obj_player) < detect_dist {
    return 1;
}
