/// sleep(ms)

if (os_type<>os_windows) show_debug_message("WARNING THIS CAN CRASH APP");

// 1,000,000 microseconds = 1 second

var s_time = get_timer();
var e_time = get_timer();

do {
    e_time = get_timer();
} until ((e_time-s_time)>argument0)



