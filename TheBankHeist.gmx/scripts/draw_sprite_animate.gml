/// draw_sprite_animate(sprite, subimg, speed, x, y, [xscale], [yscale], [rot], [colour], [alpha]);
// Everything in [] is optional
// Usage: spr_index = draw_sprite_animate(sprite, spr_index, image_speed, x, y)

var spr = argument[0];
var subimg = argument[1];
var spdimg = argument[2];
var xx = argument[3];
var yy = argument[4];
if (argument_count>5) var xscale = argument[5];
else var xscale = 1;
if (argument_count>6) var yscale = argument[6];
else var yscale = 1;
if (argument_count>7) var rot = argument[7];
else var rot = 0;
if (argument_count>8) var colour = argument[8];
else var colour = c_white;
if (argument_count>9) var alpha = argument[9];
else var alpha = 1;

draw_sprite_ext(spr, subimg, xx, yy, xscale, yscale, rot, colour, alpha);

return (subimg+spdimg)%sprite_get_number(spr);
