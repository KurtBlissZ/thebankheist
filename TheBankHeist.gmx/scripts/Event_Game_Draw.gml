/// Event_Game_Draw()
d3d_set_projection_ortho(0,0,1280,720,0);
d3d_set_hidden(false);
var xx = draw_get_alpha_test();
draw_set_alpha_test(false);
if global.ingame {
    if instance_exists(par_player)

    with par_player {
        Game_Draw_Hud();
    }
    
    with obj_youaredead {
        draw_sprite_ext(spr_youaredead,image_index,
        640,360,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
    } 
}
draw_set_alpha_test(xx);
d3d_set_hidden(true);
