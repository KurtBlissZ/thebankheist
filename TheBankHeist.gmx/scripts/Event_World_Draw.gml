#define Event_World_Draw
/// Event_World_Draw()

/*if instance_exists(obj_light_point)
d3d_set_lighting(true);
Lights_Update();
d3d_light_enable(7, true);
d3d_light_define_direction(7, 0, 0, 1, c_red);*/

// Draw models
for (var i=0; i<array_length_1d(Model); i++) 
    d3d_model_draw(Model[i], 0, 0, 0, Model_Tex[i]); 

with par_wall           World_Draw_Dynamic_Walls();
with par_wall_shootable World_Draw_Dynamic_Walls();
with par_floor          World_Draw_Dynamic_Floors();

//d3d_set_lighting(true);

with obj_street_light {
    //d3d_light_enable(0,true);
    //d3d_light_define_point(0, x+50, y-50, 700, 500, c_white);
    d3d_model_draw(global.mdl_lamp, x, y, 0, sprite_get_texture(texture_streetlight,0));
}

//d3d_set_lighting(false);

#define World_Draw_Dynamic_Floors
/// World_Draw_Dynamic_Floors()
if (DrawTypeIndex == DrawTypeDynamic)
&& (distance_to_object(obj_camera)<view_hview+64) begin

var i = background_get_texture(Tex);

d3d_draw_floor(x, y+sprite_height, Z, 
x+sprite_width, y, Z, i, sprite_width/Hrp,sprite_height/Vrp);


end ////////////////////////////////////////////////////////////////////

#define World_Draw_Dynamic_Walls
/// World_Draw_Dynamic_Walls()
if (DrawTypeIndex == DrawTypeDynamic)
|| (DrawTypeIndex == DrawTypeStatic)
draw_sprite_ext(spr_shadow, 0, x+16, y+16, image_xscale, image_yscale, 0, c_black, 0.6);

if (DrawTypeIndex == DrawTypeDynamic)
&& (distance_to_object(obj_camera)<view_hview+64) begin ////////////////

if instance_exists(obj_player) 
&& (obj_player.interact==id) {
    draw_set_color(c_red);
} else {
    draw_set_color(c_white);
}
    
var i = background_get_texture(Tex),
    ii= background_get_texture(TexTop);

d3d_draw_floor(x, y+sprite_height, Z2, 
x+sprite_width, y, Z2, ii, sprite_width/Hrp,sprite_height/Vrp);

d3d_draw_wall(x+sprite_width,y+sprite_height,Z1,
x,y+sprite_height,Z2, i,sprite_width/Hrp,Z2/Vrp);

d3d_draw_wall(x,y,Z1,
x+sprite_width,y,Z2, i, sprite_width/Hrp,Z2/Vrp);

d3d_draw_wall(x+sprite_width,y,Z1,
x+sprite_width,y+sprite_height, Z2, i, sprite_height/Hrp,Z2/Vrp);

d3d_draw_wall(x,y+sprite_height,Z1,
x,y,Z2, i, sprite_height/Hrp,Z2/Vrp);

end ////////////////////////////////////////////////////////////////////