///Menu_Add_But(name,action,[arg])
CurBut++;
Menu_Button[CurCat,CurBut] = argument[0];
Menu_Button_Action[CurCat,CurBut] = argument[1];
if (argument_count > 2) {
    Menu_Button_Arg[CurCat,CurBut] = argument[2];
} else {
    Menu_Button_Arg[CurCat,CurBut] = -1;
}
Cat_Width[CurCat] = max(string_width(argument[0]), Cat_Width[CurCat]);


