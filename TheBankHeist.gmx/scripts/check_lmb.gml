/// check_lmb(expression)

// Check left mouse button press

if mouse_check_button_released(mb_left)
&& mouse_in_window()
&& window_has_focus() {

    if (argument_count>0 && argument[0]==true)
        return true;
    
    else if click {
        click = false;
        return true;
    }

}


