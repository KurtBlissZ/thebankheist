var _delta = global.Delta;

switch (state) {
    case player.state_normal:
    Player_Step_Movement();
    Player_Step_Interact();
    Player_Step_Combat();
    break;
    
    case player.state_minigame:
    scr_player_minigame();
    break;
    //default: ;
}

time_limit--;

/// Sprite's Animation (probably not ever needed for player but)
if image_number>1 {
    if image_index>image_number
        image_index -= image_number-1;
    image_index += img_spd * _delta;
}


// Alarms
if (alarm_shoot_delay > 0) { //0
    alarm_shoot_delay -= _delta;
    if alarm_shoot_delay <= 0 {
        can_shoot = true;
    }
}

if (alarm_reloading > 0) { //1
    alarm_reloading -= _delta;
    if alarm_reloading <= 0 {
        /// Finish Reloading
        can_shoot = true;
        can_reload = true;
        var _gi = global.gun_index;
        var _at = global.gun_ammo_type[_gi]
        var _a = global.ammo[_at];
        var _add = min(global.gun_mag_size[_gi]-global.gun_mag[_gi], _a);
        global.ammo[_at] -= _add;
        global.gun_mag[_gi] += _add;
    }
}

if (alarm_set_color_back > 0) { //3
    alarm_set_color_back -= _delta;
    if alarm_set_color_back <= 0 {
        image_blend = c_white;
    }
}
