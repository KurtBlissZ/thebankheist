/// move_middle(x, rate, middle)
if (argument0 < argument2)       return min(argument0+argument1,argument2);
else if (argument0 > argument2 ) return max(argument0-argument1,argument2);

/*
    Ex: Goto X
        x = move_middle(x, hspd, goto_x);
*/


