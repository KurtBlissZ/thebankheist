#define Scr_Display_Reset
///Scr_Display_Reset(aa_level, vsync);

global.gAA    = argument0;
global.gVsync = argument1;
display_reset(global.gAA, global.gVsync);



#define Scr_Display_Vsync
///Scr_Display_Vsync(vsync)

global.gVsync = argument0;
Scr_Display_Reset(global.gAA, global.gVsync);



#define Scr_Display_AA
///Scr_Display_AA(0/2/4/8)

if (global.Can_Fullscreen) && (argument0>0)
    global.Can_Fullscreen = !show_question("Once you turn on AA you can't switch back to window mode until you turn it off and exit the game. Turn AA on?")
if global.Can_Fullscreen global.gAA = 0;
else global.gAA    = argument0;

if !global.Can_Fullscreen {
    Scr_Display_Reset(global.gAA, global.gVsync);
    window_set_fullscreen(true);
}