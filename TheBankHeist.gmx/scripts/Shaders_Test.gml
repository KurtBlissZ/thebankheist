var hardwareSupport = "Your hardware doesn't support shader - ";
    
var _shader = array(shd_bright_contrast, "shd_bright_contrast", 
  shd_emboss, "shd_emboss", shd_gaussian_horizontal, "shd_gaussian_horizontal",
  shd_gaussian_vertical, "shd_gaussian_vertical", shd_greyscale, "shd_greyscale",
  shd_invert, "shd_invert", shd_LED, "shd_LED", shd_magnify, "shd_magnify",
  shd_mosaic, "shd_mosaic", shd_posterization, "shd_posterization",
  shd_ripple, "shd_ripple", shd_scanlines, "shd_scanlines",
  shd_sepia, "shd_sepia", shd_shockwave, "shd_shockwave", shd_sketch, "shd_sketch", 
  shd_thermal, "shd_thermal", shd_wave, "shd_wave", 
  shd_radial_blur, "shd_radial_blur", shd_refraction, "shd_refraction", 
  shd_green, "shd_green", shd_blackandwhite, "shd_blackandwhite");


for (var i = 0; i < array_length_1d(_shader); i+=2)
if !shader_is_compiled(_shader[i]) {
    show_message(string(hardwareSupport)+string(_shader[i+1]));
}


