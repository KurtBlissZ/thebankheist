/// Editor_Menu_Create()
menubarheight = string_height("|");
CurCat = -1;
CurBut = -1;
Menu_Selected = -1;

Menu_Add_Cat("File");
Menu_Add_But("New", Menu_Act_New);
Menu_Add_But("Exit To Menu", Menu_Act_Exit_Menu);
Menu_Add_But("Exit Game", Menu_Act_Exit);

Menu_Add_Cat("Window");
Menu_Add_But("Reset", Menu_Act_resetWindow);
Menu_Add_But("Pick Texture", Menu_Act_Window, "Pick Texture");
Menu_Add_But("Texure Repeat", Menu_Act_Window, "Texure Repeat");

Menu_Add_Cat("Tools");
Menu_Add_But("Jump To Origin", Menu_Act_toOrigin);
Menu_Add_But("Toggle Camera", Menu_Act_Camera);
Menu_Add_But("Merge Walls", Editor_Merge_Walls);
Menu_Add_But("Merge Floors", Editor_Merge_Floors);

Menu_Add_Cat("Objects");
Menu_Add_Object(Obj_Object);
Menu_Add_Object(Obj_Object_Floor);
Menu_Add_Object(Obj_Object_Wall);

/*mode_menu_id=Menu_Add_Cat("Mode");
Menu_Update_Mode();
Menu_Add_But("None", Menu_Act_Mode_None);
Menu_Add_But("Place", Menu_Act_Mode_Place);*/

Menu_Add_Cat("Help");
Menu_Add_But("About", Menu_Act_About);
Menu_Add_But("Forums", Menu_Act_Forums);
Menu_Add_But("Project Data", Menu_Act_ProjectData);


