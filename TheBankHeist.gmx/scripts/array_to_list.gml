/// array_to_list(array)
var _array = argument0;
var _data = ds_list_create();
for (var i=0; i<array_length_1d(_array); i++) {
    ds_list_add(_data, _array[i]);
}
var _result = ds_list_write(_data);
ds_list_destroy(_data);
return _result;
