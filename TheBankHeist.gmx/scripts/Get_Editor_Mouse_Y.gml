/// Get_Editor_Mouse_Y()
if instance_exists(Obj_Editor_Camera) {
    return Obj_Editor_Camera.__y;
}

return mouse_y;

