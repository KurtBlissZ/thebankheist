/// instance_singleton([x], [y], obj)
if (argument_count==1) { 
    var xx  = 0; 
    var yy  = 0;
    var obj = argument[0];
} else {
    var xx  = argument[0]; 
    var yy  = argument[1];
    var obj = argument[2];
} if (instance_number(obj) == 0)
    return instance_create(xx, yy, obj); 
else 
    return instance_find(obj, 0);
