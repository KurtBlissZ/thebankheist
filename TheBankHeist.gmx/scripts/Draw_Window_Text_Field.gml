/// Draw_Window_Text_Field(x, y, id)
var i = argument2, txt = input_field[i];

var x1 = argument0, y1 = argument1, 
    x2 = x1+max(string_width(string(txt)), 64), y2 = y1+string_height("|"); 

var offx1 = x1 - offx, offy1 = y1 - offy, //offx & offy is instance var
    offx2 = x2 - offx, offy2 = y2 - offy; 

// select text field
if active
&& mouse_in_rectangle( x1, y1, x2, y2 ) 
&& mcp(mb_left)  {
    active_input_field = i;
    keyboard_string = string(input_field[active_input_field]);
}

// text field background
draw_set_color( iff((i==active_input_field),c_blue,c_gray) ); 
draw_rectangle( offx1, offy1, offx2, offy2, false);

// draw text input
draw_set_color( c_white ); 
draw_text( x1, y1, txt);
