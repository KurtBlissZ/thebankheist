/// Game_Preferences_Save()
ini_open("prefs.ini");
ini_write_real("Audio","MasterGain",global.AudioGain);

ini_write_real("Display","Fullscreen",window_get_fullscreen());

ini_write_real("Display","AA",global.gAA);
ini_write_real("Display","VSYNC",global.gVsync);

ini_write_real("Display","Interpolation",global.linear);

ini_write_real("Display","Brightness",var_brightness_amount);
ini_write_real("Display","Contrast",var_contrast_amount);

ini_write_real("Game","Show_Civilians",global.Show_Civilians);
ini_write_real("Game","Delta_Enabled",global.Delta_Enabled);
ini_close();
