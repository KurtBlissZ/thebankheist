/// Event_Enemy_Init()

//Stats
hp = 100;
armor = 0;
mag_size = 6;
spd = 2+random(0.5);
fov = 90;
detect_dist = 192;
fire_dist = 416;
stop_dist = 128;
backup_dist = 64;
image_xscale = 0.5;
image_yscale = 0.5;

//Init MISC
z                       = 32;
aim                     = 0;
path                    = path_add();
path_targetx            = 0;
path_targety            = 0;
state                   = en_state_idle;
state_return            = state;
path_state              = en_state_path_chase;
path_spd                = 0;
delay                   = 0;
delay_path_refresh      = 0;
delay_shoot             = 0;
delay_shoot_reaction    = -1;
delay_reload            = 0;
delay_spot              = 0;
delay_spot_set          = room_speed*0.5;
effect_hurt             = 0;
mag                     = mag_size;




