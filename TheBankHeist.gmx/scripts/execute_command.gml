#define execute_command
///execute_command(str)
if !execute_set_varibles(argument0) {
    //include_string = false;
}
 
else if (string_copy(argument0,1,3) == "rm_")
for (i=0; i<=room_last; i+=1) {
   if argument0 == room_get_name(i) {
        room_goto(i);
        break;     
   }
   else if i==room_last logmsg=' (invalid room)';
}

else switch (argument0) {
    
    // Generic Game Save and Load
    case "game_save": {
        game_save('gamesave.dat');
        show_debug_message('game_save'); 
    } break;
        
    case "game_load": {
        d3d_end();
        game_load('gamesave.dat'); 
        show_debug_message('game_load');
    } break;
    
    // Change Volume
    case "vol": audio_master_gain(get_integer('',0)); break;
    
    // Game Functions
    case "game_end": game_end(); break;  
    case "game_restart": game_restart(); break; 
    case "room_restart": room_restart(); break; 
    
    // Switch rooms
    case "room_goto_next":
    case "room_next":
        if room_exists(room_next(room)) 
        room_goto_next(); break;        
    
    case "room_goto_previous": 
    case "room_back":
        if room_exists(room_previous(room)) 
        room_goto_previous(); break;                
    
    // Stop Title Music
    case "mute_title":
        if audio_is_playing(bg_title)
        audio_stop_sound(bg_title); break;  
    
    // Hide you are dead // I forget why this is here
    case 'youaredeadhide':
        if instance_exists(obj_youaredead) obj_youaredead.visible=0;
        break;      
    
    // Goto Debug Room
    case 'debug':
        room_goto(rm_debug_menu);
        break;
    
    // Woohoo Cheat
    case "woohoo":
        show_debug_message('woohoo');
        global.dynamites = 10;
        global.gun_have[gun_pistol] = true;
        global.gun_have[gun_smg] = true;
        break;
    
    // Change Brightness and Contrast    
    case "brightness": 
        var_brightness_amount = get_integer("brightness", var_brightness_amount);
        break;
    
    case "contrast":
        var_contrast_amount = get_integer("contrast", var_contrast_amount);
        break;
    
    // Variable Checks
    case "health": {
        log='health is '+string(health)+log; 
        include_string = false;
    } break;
    
    case "score": {
        log='score is '+string(score)+log; 
        include_string = false;
    } break;    
    
    case "lives": {
        log='lives is '+string(lives)+log; 
        include_string = false;
    } break;    
    
    case "room": {
        log='room is '+string(room)+log; 
        include_string = false;
    } break;    
    
    case "fps": {
        log='fps is '+string(fps)+log; 
        include_string = false;
    } break;    
    
    // Clear
    case "clear":
    case "cls": log="cls"; break;
    
    // Invalid Command
    default: logmsg=' (invalid)'; 
}

last_command = argument0;


#define set_varible
///set_varible(varible value,varible string,command,digits);
// ex: health = set_varible('hp=',string,1)
// Used in execute_command script
var varible_return,varible,str,type,count;
varible_return = argument0;
varible = argument1;
str = argument2;
type = argument3; //digits
count = string_pos('=',str);
if string_count('=',str)==1
&& varible == string_copy(str,1,count) {   
   if type
   varible_return = real(/*string_digits*/(string_copy(str,count+1,string_width(str))));
   
   else
   varible_return = string_copy(str,count+1,string_width(str));
}
SETVAR = varible_return;
return(varible_return);

#define execute_set_varibles
/// execute_set_varibles(str)
if (room_speed != set_varible(room_speed,'room_speed=',argument0,1)) 
    room_speed = SETVAR;

else if (room != set_varible(room,'room=',argument0,1)) 
    room = SETVAR;

else if (health != set_varible(health,'health=',argument0,1))
    health = SETVAR;

else if (score != set_varible(score,'score=',argument0,1)) 
    score = SETVAR;

else if (lives != set_varible(lives,'lives=',argument0,1))
    lives = SETVAR;

else if global.ammo[ammo_pistol] != set_varible(global.ammo[ammo_pistol],'ammo=',argument0,1)
    global.ammo[ammo_pistol] = SETVAR;

else if global.armor != set_varible(global.armor,'armor=',argument0,1)
    global.armor = SETVAR;

else if global.draw_dist != set_varible(global.draw_dist,'draw_dist=',argument0,1)
    global.draw_dist = SETVAR;

else if var_brightness_amount != set_varible(var_brightness_amount,'brightness=',argument0,1)
    var_brightness_amount = SETVAR;

else if var_contrast_amount != set_varible(var_contrast_amount,'contrast=',argument0,1)
    var_contrast_amount = SETVAR;

else return true;