/// Game_Inventory()
global.dynamites = 0;

//Equiped gun
global.gun_index = gun_none;
global.gun_have[gun_none] = true;
global.gun_have[gun_pistol] = false;
global.gun_have[gun_smg] = false;

//Ammo types
global.ammo[ammo_pistol] = 0;

global.gun_sprite[gun_none] = spr_char1_idle;

//Pistol
var xx = gun_pistol;
global.gun_rate             [xx] = 0;
global.gun_mag              [xx] = 9;
global.gun_mag_size         [xx] = 9;
global.gun_sprite           [xx] = spr_char1_pistol;
global.gun_sound            [xx] = snd_gunfire;
global.gun_hud              [xx] = xx;
global.gun_ammo_type        [xx] = ammo_pistol;
global.gun_bullet           [xx] = obj_bullet1;
global.gun_bullet_damage    [xx] = 35;
global.gun_bullet_speed     [xx] = 32;
global.gun_firerate         [xx] = 40;
global.gun_effect_len       [xx] = 52;
global.gun_effect_dir       [xx] = 0;
global.gun_exp_speed        [xx] = 1.4;

//Sub-Machine Gun
xx = gun_smg;
global.gun_rate             [xx] = 0;
global.gun_mag              [xx] = 20;
global.gun_mag_size         [xx] = 20;
global.gun_sprite           [xx] = spr_char1_smg;
global.gun_sound            [xx] = snd_smg;
global.gun_hud              [xx] = xx;
global.gun_ammo_type        [xx] = ammo_pistol;
global.gun_bullet           [xx] = obj_bullet1;
global.gun_bullet_damage    [xx] = 30;
global.gun_bullet_speed     [xx] = 32;
global.gun_firerate         [xx] = 10;//5;
global.gun_effect_len       [xx] = 54;
global.gun_effect_dir       [xx] = 0;
global.gun_exp_speed        [xx] = 5;

//Record last gun
global.gun_last = xx;
