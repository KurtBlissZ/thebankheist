///en_player_can_shoot()
return (en_player_in_fov() < 10 
&& (distance_to_object(obj_player) < fire_dist)
&& en_player_in_sight());
