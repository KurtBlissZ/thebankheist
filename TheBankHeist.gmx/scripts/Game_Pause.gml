/// Game_Pause()

//Turns Pause Off
if global.pause {
    audio_resume_all();
    instance_activate_all();
    global.pause = false;
    d3d_mode(true);
    with obj_pause_menu instance_destroy();
} 
// Pauses
else {
    audio_pause_all();
    if !surface_exists(global.pause_surf)
        global.pause_surf = surface_create(1280,720);
    surface_copy(global.pause_surf,0,0,application_surface);
    instance_deactivate_all(true);
    instance_activate_object(obj_scc);
    global.pause = true;
    d3d_mode(false);
    instance_create(view_xview[0]+500,view_yview[0]+200,obj_pause_menu);
}
